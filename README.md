# Kubernetes browser client experiments

This is an exploration around generating a Kubernetes client for use in a web
browser using [OpenAPI generator](https://openapi-generator.tech).


## Usage

Create a cluster:

``` shell
kind create cluster
```

Start the proxies and the web server :

``` shell
# https://github.com/ddollar/foreman
foreman start
```

Visit https://localhost:8000.  You should see something similar to:

![Screenshot of a list of pods](screenshot.png)
