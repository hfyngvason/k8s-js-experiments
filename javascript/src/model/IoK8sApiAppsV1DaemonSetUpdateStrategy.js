/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiAppsV1RollingUpdateDaemonSet from './IoK8sApiAppsV1RollingUpdateDaemonSet';

/**
 * The IoK8sApiAppsV1DaemonSetUpdateStrategy model module.
 * @module model/IoK8sApiAppsV1DaemonSetUpdateStrategy
 * @version unversioned
 */
class IoK8sApiAppsV1DaemonSetUpdateStrategy {
    /**
     * Constructs a new <code>IoK8sApiAppsV1DaemonSetUpdateStrategy</code>.
     * DaemonSetUpdateStrategy is a struct used to control the update strategy for a DaemonSet.
     * @alias module:model/IoK8sApiAppsV1DaemonSetUpdateStrategy
     */
    constructor() { 
        
        IoK8sApiAppsV1DaemonSetUpdateStrategy.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiAppsV1DaemonSetUpdateStrategy</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiAppsV1DaemonSetUpdateStrategy} obj Optional instance to populate.
     * @return {module:model/IoK8sApiAppsV1DaemonSetUpdateStrategy} The populated <code>IoK8sApiAppsV1DaemonSetUpdateStrategy</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiAppsV1DaemonSetUpdateStrategy();

            if (data.hasOwnProperty('rollingUpdate')) {
                obj['rollingUpdate'] = IoK8sApiAppsV1RollingUpdateDaemonSet.constructFromObject(data['rollingUpdate']);
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/IoK8sApiAppsV1RollingUpdateDaemonSet} rollingUpdate
 */
IoK8sApiAppsV1DaemonSetUpdateStrategy.prototype['rollingUpdate'] = undefined;

/**
 * Type of daemon set update. Can be \"RollingUpdate\" or \"OnDelete\". Default is RollingUpdate.  
 * @member {String} type
 */
IoK8sApiAppsV1DaemonSetUpdateStrategy.prototype['type'] = undefined;






export default IoK8sApiAppsV1DaemonSetUpdateStrategy;

