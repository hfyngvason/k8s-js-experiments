/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiCoreV1NamespaceSpec model module.
 * @module model/IoK8sApiCoreV1NamespaceSpec
 * @version unversioned
 */
class IoK8sApiCoreV1NamespaceSpec {
    /**
     * Constructs a new <code>IoK8sApiCoreV1NamespaceSpec</code>.
     * NamespaceSpec describes the attributes on a Namespace.
     * @alias module:model/IoK8sApiCoreV1NamespaceSpec
     */
    constructor() { 
        
        IoK8sApiCoreV1NamespaceSpec.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiCoreV1NamespaceSpec</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiCoreV1NamespaceSpec} obj Optional instance to populate.
     * @return {module:model/IoK8sApiCoreV1NamespaceSpec} The populated <code>IoK8sApiCoreV1NamespaceSpec</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiCoreV1NamespaceSpec();

            if (data.hasOwnProperty('finalizers')) {
                obj['finalizers'] = ApiClient.convertToType(data['finalizers'], ['String']);
            }
        }
        return obj;
    }


}

/**
 * Finalizers is an opaque list of values that must be empty to permanently remove object from storage. More info: https://kubernetes.io/docs/tasks/administer-cluster/namespaces/
 * @member {Array.<String>} finalizers
 */
IoK8sApiCoreV1NamespaceSpec.prototype['finalizers'] = undefined;






export default IoK8sApiCoreV1NamespaceSpec;

