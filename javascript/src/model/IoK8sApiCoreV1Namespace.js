/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiCoreV1NamespaceSpec from './IoK8sApiCoreV1NamespaceSpec';
import IoK8sApiCoreV1NamespaceStatus from './IoK8sApiCoreV1NamespaceStatus';
import IoK8sApimachineryPkgApisMetaV1ObjectMeta from './IoK8sApimachineryPkgApisMetaV1ObjectMeta';

/**
 * The IoK8sApiCoreV1Namespace model module.
 * @module model/IoK8sApiCoreV1Namespace
 * @version unversioned
 */
class IoK8sApiCoreV1Namespace {
    /**
     * Constructs a new <code>IoK8sApiCoreV1Namespace</code>.
     * Namespace provides a scope for Names. Use of multiple namespaces is optional.
     * @alias module:model/IoK8sApiCoreV1Namespace
     */
    constructor() { 
        
        IoK8sApiCoreV1Namespace.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiCoreV1Namespace</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiCoreV1Namespace} obj Optional instance to populate.
     * @return {module:model/IoK8sApiCoreV1Namespace} The populated <code>IoK8sApiCoreV1Namespace</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiCoreV1Namespace();

            if (data.hasOwnProperty('apiVersion')) {
                obj['apiVersion'] = ApiClient.convertToType(data['apiVersion'], 'String');
            }
            if (data.hasOwnProperty('kind')) {
                obj['kind'] = ApiClient.convertToType(data['kind'], 'String');
            }
            if (data.hasOwnProperty('metadata')) {
                obj['metadata'] = IoK8sApimachineryPkgApisMetaV1ObjectMeta.constructFromObject(data['metadata']);
            }
            if (data.hasOwnProperty('spec')) {
                obj['spec'] = IoK8sApiCoreV1NamespaceSpec.constructFromObject(data['spec']);
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = IoK8sApiCoreV1NamespaceStatus.constructFromObject(data['status']);
            }
        }
        return obj;
    }


}

/**
 * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources
 * @member {String} apiVersion
 */
IoK8sApiCoreV1Namespace.prototype['apiVersion'] = undefined;

/**
 * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
 * @member {String} kind
 */
IoK8sApiCoreV1Namespace.prototype['kind'] = undefined;

/**
 * @member {module:model/IoK8sApimachineryPkgApisMetaV1ObjectMeta} metadata
 */
IoK8sApiCoreV1Namespace.prototype['metadata'] = undefined;

/**
 * @member {module:model/IoK8sApiCoreV1NamespaceSpec} spec
 */
IoK8sApiCoreV1Namespace.prototype['spec'] = undefined;

/**
 * @member {module:model/IoK8sApiCoreV1NamespaceStatus} status
 */
IoK8sApiCoreV1Namespace.prototype['status'] = undefined;






export default IoK8sApiCoreV1Namespace;

