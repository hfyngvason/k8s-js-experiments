/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiFlowcontrolV1beta3GroupSubject model module.
 * @module model/IoK8sApiFlowcontrolV1beta3GroupSubject
 * @version unversioned
 */
class IoK8sApiFlowcontrolV1beta3GroupSubject {
    /**
     * Constructs a new <code>IoK8sApiFlowcontrolV1beta3GroupSubject</code>.
     * GroupSubject holds detailed information for group-kind subject.
     * @alias module:model/IoK8sApiFlowcontrolV1beta3GroupSubject
     * @param name {String} name is the user group that matches, or \"*\" to match all user groups. See https://github.com/kubernetes/apiserver/blob/master/pkg/authentication/user/user.go for some well-known group names. Required.
     */
    constructor(name) { 
        
        IoK8sApiFlowcontrolV1beta3GroupSubject.initialize(this, name);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, name) { 
        obj['name'] = name;
    }

    /**
     * Constructs a <code>IoK8sApiFlowcontrolV1beta3GroupSubject</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiFlowcontrolV1beta3GroupSubject} obj Optional instance to populate.
     * @return {module:model/IoK8sApiFlowcontrolV1beta3GroupSubject} The populated <code>IoK8sApiFlowcontrolV1beta3GroupSubject</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiFlowcontrolV1beta3GroupSubject();

            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
        }
        return obj;
    }


}

/**
 * name is the user group that matches, or \"*\" to match all user groups. See https://github.com/kubernetes/apiserver/blob/master/pkg/authentication/user/user.go for some well-known group names. Required.
 * @member {String} name
 */
IoK8sApiFlowcontrolV1beta3GroupSubject.prototype['name'] = undefined;






export default IoK8sApiFlowcontrolV1beta3GroupSubject;

