/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiCoreV1ContainerStateTerminated model module.
 * @module model/IoK8sApiCoreV1ContainerStateTerminated
 * @version unversioned
 */
class IoK8sApiCoreV1ContainerStateTerminated {
    /**
     * Constructs a new <code>IoK8sApiCoreV1ContainerStateTerminated</code>.
     * ContainerStateTerminated is a terminated state of a container.
     * @alias module:model/IoK8sApiCoreV1ContainerStateTerminated
     * @param exitCode {Number} Exit status from the last termination of the container
     */
    constructor(exitCode) { 
        
        IoK8sApiCoreV1ContainerStateTerminated.initialize(this, exitCode);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, exitCode) { 
        obj['exitCode'] = exitCode;
    }

    /**
     * Constructs a <code>IoK8sApiCoreV1ContainerStateTerminated</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiCoreV1ContainerStateTerminated} obj Optional instance to populate.
     * @return {module:model/IoK8sApiCoreV1ContainerStateTerminated} The populated <code>IoK8sApiCoreV1ContainerStateTerminated</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiCoreV1ContainerStateTerminated();

            if (data.hasOwnProperty('containerID')) {
                obj['containerID'] = ApiClient.convertToType(data['containerID'], 'String');
            }
            if (data.hasOwnProperty('exitCode')) {
                obj['exitCode'] = ApiClient.convertToType(data['exitCode'], 'Number');
            }
            if (data.hasOwnProperty('finishedAt')) {
                obj['finishedAt'] = ApiClient.convertToType(data['finishedAt'], 'Date');
            }
            if (data.hasOwnProperty('message')) {
                obj['message'] = ApiClient.convertToType(data['message'], 'String');
            }
            if (data.hasOwnProperty('reason')) {
                obj['reason'] = ApiClient.convertToType(data['reason'], 'String');
            }
            if (data.hasOwnProperty('signal')) {
                obj['signal'] = ApiClient.convertToType(data['signal'], 'Number');
            }
            if (data.hasOwnProperty('startedAt')) {
                obj['startedAt'] = ApiClient.convertToType(data['startedAt'], 'Date');
            }
        }
        return obj;
    }


}

/**
 * Container's ID in the format '<type>://<container_id>'
 * @member {String} containerID
 */
IoK8sApiCoreV1ContainerStateTerminated.prototype['containerID'] = undefined;

/**
 * Exit status from the last termination of the container
 * @member {Number} exitCode
 */
IoK8sApiCoreV1ContainerStateTerminated.prototype['exitCode'] = undefined;

/**
 * Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers.
 * @member {Date} finishedAt
 */
IoK8sApiCoreV1ContainerStateTerminated.prototype['finishedAt'] = undefined;

/**
 * Message regarding the last termination of the container
 * @member {String} message
 */
IoK8sApiCoreV1ContainerStateTerminated.prototype['message'] = undefined;

/**
 * (brief) reason from the last termination of the container
 * @member {String} reason
 */
IoK8sApiCoreV1ContainerStateTerminated.prototype['reason'] = undefined;

/**
 * Signal from the last termination of the container
 * @member {Number} signal
 */
IoK8sApiCoreV1ContainerStateTerminated.prototype['signal'] = undefined;

/**
 * Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers.
 * @member {Date} startedAt
 */
IoK8sApiCoreV1ContainerStateTerminated.prototype['startedAt'] = undefined;






export default IoK8sApiCoreV1ContainerStateTerminated;

