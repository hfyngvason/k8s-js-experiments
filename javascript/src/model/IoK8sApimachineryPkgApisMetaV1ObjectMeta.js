/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApimachineryPkgApisMetaV1ManagedFieldsEntry from './IoK8sApimachineryPkgApisMetaV1ManagedFieldsEntry';
import IoK8sApimachineryPkgApisMetaV1OwnerReference from './IoK8sApimachineryPkgApisMetaV1OwnerReference';

/**
 * The IoK8sApimachineryPkgApisMetaV1ObjectMeta model module.
 * @module model/IoK8sApimachineryPkgApisMetaV1ObjectMeta
 * @version unversioned
 */
class IoK8sApimachineryPkgApisMetaV1ObjectMeta {
    /**
     * Constructs a new <code>IoK8sApimachineryPkgApisMetaV1ObjectMeta</code>.
     * ObjectMeta is metadata that all persisted resources must have, which includes all objects users must create.
     * @alias module:model/IoK8sApimachineryPkgApisMetaV1ObjectMeta
     */
    constructor() { 
        
        IoK8sApimachineryPkgApisMetaV1ObjectMeta.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApimachineryPkgApisMetaV1ObjectMeta</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApimachineryPkgApisMetaV1ObjectMeta} obj Optional instance to populate.
     * @return {module:model/IoK8sApimachineryPkgApisMetaV1ObjectMeta} The populated <code>IoK8sApimachineryPkgApisMetaV1ObjectMeta</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApimachineryPkgApisMetaV1ObjectMeta();

            if (data.hasOwnProperty('annotations')) {
                obj['annotations'] = ApiClient.convertToType(data['annotations'], {'String': 'String'});
            }
            if (data.hasOwnProperty('creationTimestamp')) {
                obj['creationTimestamp'] = ApiClient.convertToType(data['creationTimestamp'], 'Date');
            }
            if (data.hasOwnProperty('deletionGracePeriodSeconds')) {
                obj['deletionGracePeriodSeconds'] = ApiClient.convertToType(data['deletionGracePeriodSeconds'], 'Number');
            }
            if (data.hasOwnProperty('deletionTimestamp')) {
                obj['deletionTimestamp'] = ApiClient.convertToType(data['deletionTimestamp'], 'Date');
            }
            if (data.hasOwnProperty('finalizers')) {
                obj['finalizers'] = ApiClient.convertToType(data['finalizers'], ['String']);
            }
            if (data.hasOwnProperty('generateName')) {
                obj['generateName'] = ApiClient.convertToType(data['generateName'], 'String');
            }
            if (data.hasOwnProperty('generation')) {
                obj['generation'] = ApiClient.convertToType(data['generation'], 'Number');
            }
            if (data.hasOwnProperty('labels')) {
                obj['labels'] = ApiClient.convertToType(data['labels'], {'String': 'String'});
            }
            if (data.hasOwnProperty('managedFields')) {
                obj['managedFields'] = ApiClient.convertToType(data['managedFields'], [IoK8sApimachineryPkgApisMetaV1ManagedFieldsEntry]);
            }
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('namespace')) {
                obj['namespace'] = ApiClient.convertToType(data['namespace'], 'String');
            }
            if (data.hasOwnProperty('ownerReferences')) {
                obj['ownerReferences'] = ApiClient.convertToType(data['ownerReferences'], [IoK8sApimachineryPkgApisMetaV1OwnerReference]);
            }
            if (data.hasOwnProperty('resourceVersion')) {
                obj['resourceVersion'] = ApiClient.convertToType(data['resourceVersion'], 'String');
            }
            if (data.hasOwnProperty('selfLink')) {
                obj['selfLink'] = ApiClient.convertToType(data['selfLink'], 'String');
            }
            if (data.hasOwnProperty('uid')) {
                obj['uid'] = ApiClient.convertToType(data['uid'], 'String');
            }
        }
        return obj;
    }


}

/**
 * Annotations is an unstructured key value map stored with a resource that may be set by external tools to store and retrieve arbitrary metadata. They are not queryable and should be preserved when modifying objects. More info: http://kubernetes.io/docs/user-guide/annotations
 * @member {Object.<String, String>} annotations
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['annotations'] = undefined;

/**
 * Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers.
 * @member {Date} creationTimestamp
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['creationTimestamp'] = undefined;

/**
 * Number of seconds allowed for this object to gracefully terminate before it will be removed from the system. Only set when deletionTimestamp is also set. May only be shortened. Read-only.
 * @member {Number} deletionGracePeriodSeconds
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['deletionGracePeriodSeconds'] = undefined;

/**
 * Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers.
 * @member {Date} deletionTimestamp
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['deletionTimestamp'] = undefined;

/**
 * Must be empty before the object is deleted from the registry. Each entry is an identifier for the responsible component that will remove the entry from the list. If the deletionTimestamp of the object is non-nil, entries in this list can only be removed. Finalizers may be processed and removed in any order.  Order is NOT enforced because it introduces significant risk of stuck finalizers. finalizers is a shared field, any actor with permission can reorder it. If the finalizer list is processed in order, then this can lead to a situation in which the component responsible for the first finalizer in the list is waiting for a signal (field value, external system, or other) produced by a component responsible for a finalizer later in the list, resulting in a deadlock. Without enforced ordering finalizers are free to order amongst themselves and are not vulnerable to ordering changes in the list.
 * @member {Array.<String>} finalizers
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['finalizers'] = undefined;

/**
 * GenerateName is an optional prefix, used by the server, to generate a unique name ONLY IF the Name field has not been provided. If this field is used, the name returned to the client will be different than the name passed. This value will also be combined with a unique suffix. The provided value has the same validation rules as the Name field, and may be truncated by the length of the suffix required to make the value unique on the server.  If this field is specified and the generated name exists, the server will return a 409.  Applied only if Name is not specified. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#idempotency
 * @member {String} generateName
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['generateName'] = undefined;

/**
 * A sequence number representing a specific generation of the desired state. Populated by the system. Read-only.
 * @member {Number} generation
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['generation'] = undefined;

/**
 * Map of string keys and values that can be used to organize and categorize (scope and select) objects. May match selectors of replication controllers and services. More info: http://kubernetes.io/docs/user-guide/labels
 * @member {Object.<String, String>} labels
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['labels'] = undefined;

/**
 * ManagedFields maps workflow-id and version to the set of fields that are managed by that workflow. This is mostly for internal housekeeping, and users typically shouldn't need to set or understand this field. A workflow can be the user's name, a controller's name, or the name of a specific apply path like \"ci-cd\". The set of fields is always in the version that the workflow used when modifying the object.
 * @member {Array.<module:model/IoK8sApimachineryPkgApisMetaV1ManagedFieldsEntry>} managedFields
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['managedFields'] = undefined;

/**
 * Name must be unique within a namespace. Is required when creating resources, although some resources may allow a client to request the generation of an appropriate name automatically. Name is primarily intended for creation idempotence and configuration definition. Cannot be updated. More info: http://kubernetes.io/docs/user-guide/identifiers#names
 * @member {String} name
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['name'] = undefined;

/**
 * Namespace defines the space within which each name must be unique. An empty namespace is equivalent to the \"default\" namespace, but \"default\" is the canonical representation. Not all objects are required to be scoped to a namespace - the value of this field for those objects will be empty.  Must be a DNS_LABEL. Cannot be updated. More info: http://kubernetes.io/docs/user-guide/namespaces
 * @member {String} namespace
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['namespace'] = undefined;

/**
 * List of objects depended by this object. If ALL objects in the list have been deleted, this object will be garbage collected. If this object is managed by a controller, then an entry in this list will point to this controller, with the controller field set to true. There cannot be more than one managing controller.
 * @member {Array.<module:model/IoK8sApimachineryPkgApisMetaV1OwnerReference>} ownerReferences
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['ownerReferences'] = undefined;

/**
 * An opaque value that represents the internal version of this object that can be used by clients to determine when objects have changed. May be used for optimistic concurrency, change detection, and the watch operation on a resource or set of resources. Clients must treat these values as opaque and passed unmodified back to the server. They may only be valid for a particular resource or set of resources.  Populated by the system. Read-only. Value must be treated as opaque by clients and . More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#concurrency-control-and-consistency
 * @member {String} resourceVersion
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['resourceVersion'] = undefined;

/**
 * Deprecated: selfLink is a legacy read-only field that is no longer populated by the system.
 * @member {String} selfLink
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['selfLink'] = undefined;

/**
 * UID is the unique in time and space value for this object. It is typically generated by the server on successful creation of a resource and is not allowed to change on PUT operations.  Populated by the system. Read-only. More info: http://kubernetes.io/docs/user-guide/identifiers#uids
 * @member {String} uid
 */
IoK8sApimachineryPkgApisMetaV1ObjectMeta.prototype['uid'] = undefined;






export default IoK8sApimachineryPkgApisMetaV1ObjectMeta;

