/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApimachineryPkgApisMetaV1StatusCause from './IoK8sApimachineryPkgApisMetaV1StatusCause';

/**
 * The IoK8sApimachineryPkgApisMetaV1StatusDetails model module.
 * @module model/IoK8sApimachineryPkgApisMetaV1StatusDetails
 * @version unversioned
 */
class IoK8sApimachineryPkgApisMetaV1StatusDetails {
    /**
     * Constructs a new <code>IoK8sApimachineryPkgApisMetaV1StatusDetails</code>.
     * StatusDetails is a set of additional properties that MAY be set by the server to provide additional information about a response. The Reason field of a Status object defines what attributes will be set. Clients must ignore fields that do not match the defined type of each attribute, and should assume that any attribute may be empty, invalid, or under defined.
     * @alias module:model/IoK8sApimachineryPkgApisMetaV1StatusDetails
     */
    constructor() { 
        
        IoK8sApimachineryPkgApisMetaV1StatusDetails.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApimachineryPkgApisMetaV1StatusDetails</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApimachineryPkgApisMetaV1StatusDetails} obj Optional instance to populate.
     * @return {module:model/IoK8sApimachineryPkgApisMetaV1StatusDetails} The populated <code>IoK8sApimachineryPkgApisMetaV1StatusDetails</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApimachineryPkgApisMetaV1StatusDetails();

            if (data.hasOwnProperty('causes')) {
                obj['causes'] = ApiClient.convertToType(data['causes'], [IoK8sApimachineryPkgApisMetaV1StatusCause]);
            }
            if (data.hasOwnProperty('group')) {
                obj['group'] = ApiClient.convertToType(data['group'], 'String');
            }
            if (data.hasOwnProperty('kind')) {
                obj['kind'] = ApiClient.convertToType(data['kind'], 'String');
            }
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('retryAfterSeconds')) {
                obj['retryAfterSeconds'] = ApiClient.convertToType(data['retryAfterSeconds'], 'Number');
            }
            if (data.hasOwnProperty('uid')) {
                obj['uid'] = ApiClient.convertToType(data['uid'], 'String');
            }
        }
        return obj;
    }


}

/**
 * The Causes array includes more details associated with the StatusReason failure. Not all StatusReasons may provide detailed causes.
 * @member {Array.<module:model/IoK8sApimachineryPkgApisMetaV1StatusCause>} causes
 */
IoK8sApimachineryPkgApisMetaV1StatusDetails.prototype['causes'] = undefined;

/**
 * The group attribute of the resource associated with the status StatusReason.
 * @member {String} group
 */
IoK8sApimachineryPkgApisMetaV1StatusDetails.prototype['group'] = undefined;

/**
 * The kind attribute of the resource associated with the status StatusReason. On some operations may differ from the requested resource Kind. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
 * @member {String} kind
 */
IoK8sApimachineryPkgApisMetaV1StatusDetails.prototype['kind'] = undefined;

/**
 * The name attribute of the resource associated with the status StatusReason (when there is a single name which can be described).
 * @member {String} name
 */
IoK8sApimachineryPkgApisMetaV1StatusDetails.prototype['name'] = undefined;

/**
 * If specified, the time in seconds before the operation should be retried. Some errors may indicate the client must take an alternate action - for those errors this field may indicate how long to wait before taking the alternate action.
 * @member {Number} retryAfterSeconds
 */
IoK8sApimachineryPkgApisMetaV1StatusDetails.prototype['retryAfterSeconds'] = undefined;

/**
 * UID of the resource. (when there is a single resource which can be described). More info: http://kubernetes.io/docs/user-guide/identifiers#uids
 * @member {String} uid
 */
IoK8sApimachineryPkgApisMetaV1StatusDetails.prototype['uid'] = undefined;






export default IoK8sApimachineryPkgApisMetaV1StatusDetails;

