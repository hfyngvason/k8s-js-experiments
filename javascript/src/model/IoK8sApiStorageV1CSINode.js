/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiStorageV1CSINodeSpec from './IoK8sApiStorageV1CSINodeSpec';
import IoK8sApimachineryPkgApisMetaV1ObjectMeta from './IoK8sApimachineryPkgApisMetaV1ObjectMeta';

/**
 * The IoK8sApiStorageV1CSINode model module.
 * @module model/IoK8sApiStorageV1CSINode
 * @version unversioned
 */
class IoK8sApiStorageV1CSINode {
    /**
     * Constructs a new <code>IoK8sApiStorageV1CSINode</code>.
     * CSINode holds information about all CSI drivers installed on a node. CSI drivers do not need to create the CSINode object directly. As long as they use the node-driver-registrar sidecar container, the kubelet will automatically populate the CSINode object for the CSI driver as part of kubelet plugin registration. CSINode has the same name as a node. If the object is missing, it means either there are no CSI Drivers available on the node, or the Kubelet version is low enough that it doesn&#39;t create this object. CSINode has an OwnerReference that points to the corresponding node object.
     * @alias module:model/IoK8sApiStorageV1CSINode
     * @param spec {module:model/IoK8sApiStorageV1CSINodeSpec} 
     */
    constructor(spec) { 
        
        IoK8sApiStorageV1CSINode.initialize(this, spec);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, spec) { 
        obj['spec'] = spec;
    }

    /**
     * Constructs a <code>IoK8sApiStorageV1CSINode</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiStorageV1CSINode} obj Optional instance to populate.
     * @return {module:model/IoK8sApiStorageV1CSINode} The populated <code>IoK8sApiStorageV1CSINode</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiStorageV1CSINode();

            if (data.hasOwnProperty('apiVersion')) {
                obj['apiVersion'] = ApiClient.convertToType(data['apiVersion'], 'String');
            }
            if (data.hasOwnProperty('kind')) {
                obj['kind'] = ApiClient.convertToType(data['kind'], 'String');
            }
            if (data.hasOwnProperty('metadata')) {
                obj['metadata'] = IoK8sApimachineryPkgApisMetaV1ObjectMeta.constructFromObject(data['metadata']);
            }
            if (data.hasOwnProperty('spec')) {
                obj['spec'] = IoK8sApiStorageV1CSINodeSpec.constructFromObject(data['spec']);
            }
        }
        return obj;
    }


}

/**
 * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources
 * @member {String} apiVersion
 */
IoK8sApiStorageV1CSINode.prototype['apiVersion'] = undefined;

/**
 * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
 * @member {String} kind
 */
IoK8sApiStorageV1CSINode.prototype['kind'] = undefined;

/**
 * @member {module:model/IoK8sApimachineryPkgApisMetaV1ObjectMeta} metadata
 */
IoK8sApiStorageV1CSINode.prototype['metadata'] = undefined;

/**
 * @member {module:model/IoK8sApiStorageV1CSINodeSpec} spec
 */
IoK8sApiStorageV1CSINode.prototype['spec'] = undefined;






export default IoK8sApiStorageV1CSINode;

