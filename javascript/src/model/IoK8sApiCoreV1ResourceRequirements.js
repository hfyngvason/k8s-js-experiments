/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiCoreV1ResourceRequirements model module.
 * @module model/IoK8sApiCoreV1ResourceRequirements
 * @version unversioned
 */
class IoK8sApiCoreV1ResourceRequirements {
    /**
     * Constructs a new <code>IoK8sApiCoreV1ResourceRequirements</code>.
     * ResourceRequirements describes the compute resource requirements.
     * @alias module:model/IoK8sApiCoreV1ResourceRequirements
     */
    constructor() { 
        
        IoK8sApiCoreV1ResourceRequirements.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiCoreV1ResourceRequirements</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiCoreV1ResourceRequirements} obj Optional instance to populate.
     * @return {module:model/IoK8sApiCoreV1ResourceRequirements} The populated <code>IoK8sApiCoreV1ResourceRequirements</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiCoreV1ResourceRequirements();

            if (data.hasOwnProperty('limits')) {
                obj['limits'] = ApiClient.convertToType(data['limits'], {'String': 'String'});
            }
            if (data.hasOwnProperty('requests')) {
                obj['requests'] = ApiClient.convertToType(data['requests'], {'String': 'String'});
            }
        }
        return obj;
    }


}

/**
 * Limits describes the maximum amount of compute resources allowed. More info: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/
 * @member {Object.<String, String>} limits
 */
IoK8sApiCoreV1ResourceRequirements.prototype['limits'] = undefined;

/**
 * Requests describes the minimum amount of compute resources required. If Requests is omitted for a container, it defaults to Limits if that is explicitly specified, otherwise to an implementation-defined value. More info: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/
 * @member {Object.<String, String>} requests
 */
IoK8sApiCoreV1ResourceRequirements.prototype['requests'] = undefined;






export default IoK8sApiCoreV1ResourceRequirements;

