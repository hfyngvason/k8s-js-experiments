/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiCoreV1TypedLocalObjectReference from './IoK8sApiCoreV1TypedLocalObjectReference';
import IoK8sApiNetworkingV1IngressServiceBackend from './IoK8sApiNetworkingV1IngressServiceBackend';

/**
 * The IoK8sApiNetworkingV1IngressBackend model module.
 * @module model/IoK8sApiNetworkingV1IngressBackend
 * @version unversioned
 */
class IoK8sApiNetworkingV1IngressBackend {
    /**
     * Constructs a new <code>IoK8sApiNetworkingV1IngressBackend</code>.
     * IngressBackend describes all endpoints for a given service and port.
     * @alias module:model/IoK8sApiNetworkingV1IngressBackend
     */
    constructor() { 
        
        IoK8sApiNetworkingV1IngressBackend.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiNetworkingV1IngressBackend</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiNetworkingV1IngressBackend} obj Optional instance to populate.
     * @return {module:model/IoK8sApiNetworkingV1IngressBackend} The populated <code>IoK8sApiNetworkingV1IngressBackend</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiNetworkingV1IngressBackend();

            if (data.hasOwnProperty('resource')) {
                obj['resource'] = IoK8sApiCoreV1TypedLocalObjectReference.constructFromObject(data['resource']);
            }
            if (data.hasOwnProperty('service')) {
                obj['service'] = IoK8sApiNetworkingV1IngressServiceBackend.constructFromObject(data['service']);
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/IoK8sApiCoreV1TypedLocalObjectReference} resource
 */
IoK8sApiNetworkingV1IngressBackend.prototype['resource'] = undefined;

/**
 * @member {module:model/IoK8sApiNetworkingV1IngressServiceBackend} service
 */
IoK8sApiNetworkingV1IngressBackend.prototype['service'] = undefined;






export default IoK8sApiNetworkingV1IngressBackend;

