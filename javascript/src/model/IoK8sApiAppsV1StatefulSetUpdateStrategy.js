/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiAppsV1RollingUpdateStatefulSetStrategy from './IoK8sApiAppsV1RollingUpdateStatefulSetStrategy';

/**
 * The IoK8sApiAppsV1StatefulSetUpdateStrategy model module.
 * @module model/IoK8sApiAppsV1StatefulSetUpdateStrategy
 * @version unversioned
 */
class IoK8sApiAppsV1StatefulSetUpdateStrategy {
    /**
     * Constructs a new <code>IoK8sApiAppsV1StatefulSetUpdateStrategy</code>.
     * StatefulSetUpdateStrategy indicates the strategy that the StatefulSet controller will use to perform updates. It includes any additional parameters necessary to perform the update for the indicated strategy.
     * @alias module:model/IoK8sApiAppsV1StatefulSetUpdateStrategy
     */
    constructor() { 
        
        IoK8sApiAppsV1StatefulSetUpdateStrategy.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiAppsV1StatefulSetUpdateStrategy</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiAppsV1StatefulSetUpdateStrategy} obj Optional instance to populate.
     * @return {module:model/IoK8sApiAppsV1StatefulSetUpdateStrategy} The populated <code>IoK8sApiAppsV1StatefulSetUpdateStrategy</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiAppsV1StatefulSetUpdateStrategy();

            if (data.hasOwnProperty('rollingUpdate')) {
                obj['rollingUpdate'] = IoK8sApiAppsV1RollingUpdateStatefulSetStrategy.constructFromObject(data['rollingUpdate']);
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/IoK8sApiAppsV1RollingUpdateStatefulSetStrategy} rollingUpdate
 */
IoK8sApiAppsV1StatefulSetUpdateStrategy.prototype['rollingUpdate'] = undefined;

/**
 * Type indicates the type of the StatefulSetUpdateStrategy. Default is RollingUpdate.  
 * @member {String} type
 */
IoK8sApiAppsV1StatefulSetUpdateStrategy.prototype['type'] = undefined;






export default IoK8sApiAppsV1StatefulSetUpdateStrategy;

