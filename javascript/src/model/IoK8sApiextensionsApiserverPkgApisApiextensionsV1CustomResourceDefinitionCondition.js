/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition model module.
 * @module model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition
 * @version unversioned
 */
class IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition {
    /**
     * Constructs a new <code>IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition</code>.
     * CustomResourceDefinitionCondition contains details for the current condition of this pod.
     * @alias module:model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition
     * @param status {String} status is the status of the condition. Can be True, False, Unknown.
     * @param type {String} type is the type of the condition. Types include Established, NamesAccepted and Terminating.
     */
    constructor(status, type) { 
        
        IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition.initialize(this, status, type);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, status, type) { 
        obj['status'] = status;
        obj['type'] = type;
    }

    /**
     * Constructs a <code>IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition} obj Optional instance to populate.
     * @return {module:model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition} The populated <code>IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition();

            if (data.hasOwnProperty('lastTransitionTime')) {
                obj['lastTransitionTime'] = ApiClient.convertToType(data['lastTransitionTime'], 'Date');
            }
            if (data.hasOwnProperty('message')) {
                obj['message'] = ApiClient.convertToType(data['message'], 'String');
            }
            if (data.hasOwnProperty('reason')) {
                obj['reason'] = ApiClient.convertToType(data['reason'], 'String');
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = ApiClient.convertToType(data['status'], 'String');
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'String');
            }
        }
        return obj;
    }


}

/**
 * Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers.
 * @member {Date} lastTransitionTime
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition.prototype['lastTransitionTime'] = undefined;

/**
 * message is a human-readable message indicating details about last transition.
 * @member {String} message
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition.prototype['message'] = undefined;

/**
 * reason is a unique, one-word, CamelCase reason for the condition's last transition.
 * @member {String} reason
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition.prototype['reason'] = undefined;

/**
 * status is the status of the condition. Can be True, False, Unknown.
 * @member {String} status
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition.prototype['status'] = undefined;

/**
 * type is the type of the condition. Types include Established, NamesAccepted and Terminating.
 * @member {String} type
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition.prototype['type'] = undefined;






export default IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionCondition;

