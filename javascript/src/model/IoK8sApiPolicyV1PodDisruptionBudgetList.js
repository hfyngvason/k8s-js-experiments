/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiPolicyV1PodDisruptionBudget from './IoK8sApiPolicyV1PodDisruptionBudget';
import IoK8sApimachineryPkgApisMetaV1ListMeta from './IoK8sApimachineryPkgApisMetaV1ListMeta';

/**
 * The IoK8sApiPolicyV1PodDisruptionBudgetList model module.
 * @module model/IoK8sApiPolicyV1PodDisruptionBudgetList
 * @version unversioned
 */
class IoK8sApiPolicyV1PodDisruptionBudgetList {
    /**
     * Constructs a new <code>IoK8sApiPolicyV1PodDisruptionBudgetList</code>.
     * PodDisruptionBudgetList is a collection of PodDisruptionBudgets.
     * @alias module:model/IoK8sApiPolicyV1PodDisruptionBudgetList
     * @param items {Array.<module:model/IoK8sApiPolicyV1PodDisruptionBudget>} Items is a list of PodDisruptionBudgets
     */
    constructor(items) { 
        
        IoK8sApiPolicyV1PodDisruptionBudgetList.initialize(this, items);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, items) { 
        obj['items'] = items;
    }

    /**
     * Constructs a <code>IoK8sApiPolicyV1PodDisruptionBudgetList</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiPolicyV1PodDisruptionBudgetList} obj Optional instance to populate.
     * @return {module:model/IoK8sApiPolicyV1PodDisruptionBudgetList} The populated <code>IoK8sApiPolicyV1PodDisruptionBudgetList</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiPolicyV1PodDisruptionBudgetList();

            if (data.hasOwnProperty('apiVersion')) {
                obj['apiVersion'] = ApiClient.convertToType(data['apiVersion'], 'String');
            }
            if (data.hasOwnProperty('items')) {
                obj['items'] = ApiClient.convertToType(data['items'], [IoK8sApiPolicyV1PodDisruptionBudget]);
            }
            if (data.hasOwnProperty('kind')) {
                obj['kind'] = ApiClient.convertToType(data['kind'], 'String');
            }
            if (data.hasOwnProperty('metadata')) {
                obj['metadata'] = IoK8sApimachineryPkgApisMetaV1ListMeta.constructFromObject(data['metadata']);
            }
        }
        return obj;
    }


}

/**
 * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources
 * @member {String} apiVersion
 */
IoK8sApiPolicyV1PodDisruptionBudgetList.prototype['apiVersion'] = undefined;

/**
 * Items is a list of PodDisruptionBudgets
 * @member {Array.<module:model/IoK8sApiPolicyV1PodDisruptionBudget>} items
 */
IoK8sApiPolicyV1PodDisruptionBudgetList.prototype['items'] = undefined;

/**
 * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
 * @member {String} kind
 */
IoK8sApiPolicyV1PodDisruptionBudgetList.prototype['kind'] = undefined;

/**
 * @member {module:model/IoK8sApimachineryPkgApisMetaV1ListMeta} metadata
 */
IoK8sApiPolicyV1PodDisruptionBudgetList.prototype['metadata'] = undefined;






export default IoK8sApiPolicyV1PodDisruptionBudgetList;

