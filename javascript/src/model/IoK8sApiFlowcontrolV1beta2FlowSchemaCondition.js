/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiFlowcontrolV1beta2FlowSchemaCondition model module.
 * @module model/IoK8sApiFlowcontrolV1beta2FlowSchemaCondition
 * @version unversioned
 */
class IoK8sApiFlowcontrolV1beta2FlowSchemaCondition {
    /**
     * Constructs a new <code>IoK8sApiFlowcontrolV1beta2FlowSchemaCondition</code>.
     * FlowSchemaCondition describes conditions for a FlowSchema.
     * @alias module:model/IoK8sApiFlowcontrolV1beta2FlowSchemaCondition
     */
    constructor() { 
        
        IoK8sApiFlowcontrolV1beta2FlowSchemaCondition.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiFlowcontrolV1beta2FlowSchemaCondition</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiFlowcontrolV1beta2FlowSchemaCondition} obj Optional instance to populate.
     * @return {module:model/IoK8sApiFlowcontrolV1beta2FlowSchemaCondition} The populated <code>IoK8sApiFlowcontrolV1beta2FlowSchemaCondition</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiFlowcontrolV1beta2FlowSchemaCondition();

            if (data.hasOwnProperty('lastTransitionTime')) {
                obj['lastTransitionTime'] = ApiClient.convertToType(data['lastTransitionTime'], 'Date');
            }
            if (data.hasOwnProperty('message')) {
                obj['message'] = ApiClient.convertToType(data['message'], 'String');
            }
            if (data.hasOwnProperty('reason')) {
                obj['reason'] = ApiClient.convertToType(data['reason'], 'String');
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = ApiClient.convertToType(data['status'], 'String');
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'String');
            }
        }
        return obj;
    }


}

/**
 * Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers.
 * @member {Date} lastTransitionTime
 */
IoK8sApiFlowcontrolV1beta2FlowSchemaCondition.prototype['lastTransitionTime'] = undefined;

/**
 * `message` is a human-readable message indicating details about last transition.
 * @member {String} message
 */
IoK8sApiFlowcontrolV1beta2FlowSchemaCondition.prototype['message'] = undefined;

/**
 * `reason` is a unique, one-word, CamelCase reason for the condition's last transition.
 * @member {String} reason
 */
IoK8sApiFlowcontrolV1beta2FlowSchemaCondition.prototype['reason'] = undefined;

/**
 * `status` is the status of the condition. Can be True, False, Unknown. Required.
 * @member {String} status
 */
IoK8sApiFlowcontrolV1beta2FlowSchemaCondition.prototype['status'] = undefined;

/**
 * `type` is the type of the condition. Required.
 * @member {String} type
 */
IoK8sApiFlowcontrolV1beta2FlowSchemaCondition.prototype['type'] = undefined;






export default IoK8sApiFlowcontrolV1beta2FlowSchemaCondition;

