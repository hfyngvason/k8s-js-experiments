/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresourceScale from './IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresourceScale';

/**
 * The IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources model module.
 * @module model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources
 * @version unversioned
 */
class IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources {
    /**
     * Constructs a new <code>IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources</code>.
     * CustomResourceSubresources defines the status and scale subresources for CustomResources.
     * @alias module:model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources
     */
    constructor() { 
        
        IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources} obj Optional instance to populate.
     * @return {module:model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources} The populated <code>IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources();

            if (data.hasOwnProperty('scale')) {
                obj['scale'] = IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresourceScale.constructFromObject(data['scale']);
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = ApiClient.convertToType(data['status'], Object);
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresourceScale} scale
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources.prototype['scale'] = undefined;

/**
 * CustomResourceSubresourceStatus defines how to serve the status subresource for CustomResources. Status is represented by the `.status` JSON path inside of a CustomResource. When set, * exposes a /status subresource for the custom resource * PUT requests to the /status subresource take a custom resource object, and ignore changes to anything except the status stanza * PUT/POST/PATCH requests to the custom resource ignore changes to the status stanza
 * @member {Object} status
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources.prototype['status'] = undefined;






export default IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceSubresources;

