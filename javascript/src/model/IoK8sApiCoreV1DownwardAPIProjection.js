/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiCoreV1DownwardAPIVolumeFile from './IoK8sApiCoreV1DownwardAPIVolumeFile';

/**
 * The IoK8sApiCoreV1DownwardAPIProjection model module.
 * @module model/IoK8sApiCoreV1DownwardAPIProjection
 * @version unversioned
 */
class IoK8sApiCoreV1DownwardAPIProjection {
    /**
     * Constructs a new <code>IoK8sApiCoreV1DownwardAPIProjection</code>.
     * Represents downward API info for projecting into a projected volume. Note that this is identical to a downwardAPI volume source without the default mode.
     * @alias module:model/IoK8sApiCoreV1DownwardAPIProjection
     */
    constructor() { 
        
        IoK8sApiCoreV1DownwardAPIProjection.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiCoreV1DownwardAPIProjection</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiCoreV1DownwardAPIProjection} obj Optional instance to populate.
     * @return {module:model/IoK8sApiCoreV1DownwardAPIProjection} The populated <code>IoK8sApiCoreV1DownwardAPIProjection</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiCoreV1DownwardAPIProjection();

            if (data.hasOwnProperty('items')) {
                obj['items'] = ApiClient.convertToType(data['items'], [IoK8sApiCoreV1DownwardAPIVolumeFile]);
            }
        }
        return obj;
    }


}

/**
 * Items is a list of DownwardAPIVolume file
 * @member {Array.<module:model/IoK8sApiCoreV1DownwardAPIVolumeFile>} items
 */
IoK8sApiCoreV1DownwardAPIProjection.prototype['items'] = undefined;






export default IoK8sApiCoreV1DownwardAPIProjection;

