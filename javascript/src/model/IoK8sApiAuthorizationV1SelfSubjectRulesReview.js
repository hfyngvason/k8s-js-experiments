/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiAuthorizationV1SelfSubjectRulesReviewSpec from './IoK8sApiAuthorizationV1SelfSubjectRulesReviewSpec';
import IoK8sApiAuthorizationV1SubjectRulesReviewStatus from './IoK8sApiAuthorizationV1SubjectRulesReviewStatus';
import IoK8sApimachineryPkgApisMetaV1ObjectMeta from './IoK8sApimachineryPkgApisMetaV1ObjectMeta';

/**
 * The IoK8sApiAuthorizationV1SelfSubjectRulesReview model module.
 * @module model/IoK8sApiAuthorizationV1SelfSubjectRulesReview
 * @version unversioned
 */
class IoK8sApiAuthorizationV1SelfSubjectRulesReview {
    /**
     * Constructs a new <code>IoK8sApiAuthorizationV1SelfSubjectRulesReview</code>.
     * SelfSubjectRulesReview enumerates the set of actions the current user can perform within a namespace. The returned list of actions may be incomplete depending on the server&#39;s authorization mode, and any errors experienced during the evaluation. SelfSubjectRulesReview should be used by UIs to show/hide actions, or to quickly let an end user reason about their permissions. It should NOT Be used by external systems to drive authorization decisions as this raises confused deputy, cache lifetime/revocation, and correctness concerns. SubjectAccessReview, and LocalAccessReview are the correct way to defer authorization decisions to the API server.
     * @alias module:model/IoK8sApiAuthorizationV1SelfSubjectRulesReview
     * @param spec {module:model/IoK8sApiAuthorizationV1SelfSubjectRulesReviewSpec} 
     */
    constructor(spec) { 
        
        IoK8sApiAuthorizationV1SelfSubjectRulesReview.initialize(this, spec);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, spec) { 
        obj['spec'] = spec;
    }

    /**
     * Constructs a <code>IoK8sApiAuthorizationV1SelfSubjectRulesReview</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiAuthorizationV1SelfSubjectRulesReview} obj Optional instance to populate.
     * @return {module:model/IoK8sApiAuthorizationV1SelfSubjectRulesReview} The populated <code>IoK8sApiAuthorizationV1SelfSubjectRulesReview</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiAuthorizationV1SelfSubjectRulesReview();

            if (data.hasOwnProperty('apiVersion')) {
                obj['apiVersion'] = ApiClient.convertToType(data['apiVersion'], 'String');
            }
            if (data.hasOwnProperty('kind')) {
                obj['kind'] = ApiClient.convertToType(data['kind'], 'String');
            }
            if (data.hasOwnProperty('metadata')) {
                obj['metadata'] = IoK8sApimachineryPkgApisMetaV1ObjectMeta.constructFromObject(data['metadata']);
            }
            if (data.hasOwnProperty('spec')) {
                obj['spec'] = IoK8sApiAuthorizationV1SelfSubjectRulesReviewSpec.constructFromObject(data['spec']);
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = IoK8sApiAuthorizationV1SubjectRulesReviewStatus.constructFromObject(data['status']);
            }
        }
        return obj;
    }


}

/**
 * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources
 * @member {String} apiVersion
 */
IoK8sApiAuthorizationV1SelfSubjectRulesReview.prototype['apiVersion'] = undefined;

/**
 * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
 * @member {String} kind
 */
IoK8sApiAuthorizationV1SelfSubjectRulesReview.prototype['kind'] = undefined;

/**
 * @member {module:model/IoK8sApimachineryPkgApisMetaV1ObjectMeta} metadata
 */
IoK8sApiAuthorizationV1SelfSubjectRulesReview.prototype['metadata'] = undefined;

/**
 * @member {module:model/IoK8sApiAuthorizationV1SelfSubjectRulesReviewSpec} spec
 */
IoK8sApiAuthorizationV1SelfSubjectRulesReview.prototype['spec'] = undefined;

/**
 * @member {module:model/IoK8sApiAuthorizationV1SubjectRulesReviewStatus} status
 */
IoK8sApiAuthorizationV1SelfSubjectRulesReview.prototype['status'] = undefined;






export default IoK8sApiAuthorizationV1SelfSubjectRulesReview;

