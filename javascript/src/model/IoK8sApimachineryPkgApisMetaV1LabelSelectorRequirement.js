/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement model module.
 * @module model/IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement
 * @version unversioned
 */
class IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement {
    /**
     * Constructs a new <code>IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement</code>.
     * A label selector requirement is a selector that contains values, a key, and an operator that relates the key and values.
     * @alias module:model/IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement
     * @param key {String} key is the label key that the selector applies to.
     * @param operator {String} operator represents a key's relationship to a set of values. Valid operators are In, NotIn, Exists and DoesNotExist.
     */
    constructor(key, operator) { 
        
        IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement.initialize(this, key, operator);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, key, operator) { 
        obj['key'] = key;
        obj['operator'] = operator;
    }

    /**
     * Constructs a <code>IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement} obj Optional instance to populate.
     * @return {module:model/IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement} The populated <code>IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement();

            if (data.hasOwnProperty('key')) {
                obj['key'] = ApiClient.convertToType(data['key'], 'String');
            }
            if (data.hasOwnProperty('operator')) {
                obj['operator'] = ApiClient.convertToType(data['operator'], 'String');
            }
            if (data.hasOwnProperty('values')) {
                obj['values'] = ApiClient.convertToType(data['values'], ['String']);
            }
        }
        return obj;
    }


}

/**
 * key is the label key that the selector applies to.
 * @member {String} key
 */
IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement.prototype['key'] = undefined;

/**
 * operator represents a key's relationship to a set of values. Valid operators are In, NotIn, Exists and DoesNotExist.
 * @member {String} operator
 */
IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement.prototype['operator'] = undefined;

/**
 * values is an array of string values. If the operator is In or NotIn, the values array must be non-empty. If the operator is Exists or DoesNotExist, the values array must be empty. This array is replaced during a strategic merge patch.
 * @member {Array.<String>} values
 */
IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement.prototype['values'] = undefined;






export default IoK8sApimachineryPkgApisMetaV1LabelSelectorRequirement;

