/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement model module.
 * @module model/IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement
 * @version unversioned
 */
class IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement {
    /**
     * Constructs a new <code>IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement</code>.
     * PodFailurePolicyOnExitCodesRequirement describes the requirement for handling a failed pod based on its container exit codes. In particular, it lookups the .state.terminated.exitCode for each app container and init container status, represented by the .status.containerStatuses and .status.initContainerStatuses fields in the Pod status, respectively. Containers completed with success (exit code 0) are excluded from the requirement check.
     * @alias module:model/IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement
     * @param operator {String} Represents the relationship between the container exit code(s) and the specified values. Containers completed with success (exit code 0) are excluded from the requirement check. Possible values are: - In: the requirement is satisfied if at least one container exit code   (might be multiple if there are multiple containers not restricted   by the 'containerName' field) is in the set of specified values. - NotIn: the requirement is satisfied if at least one container exit code   (might be multiple if there are multiple containers not restricted   by the 'containerName' field) is not in the set of specified values. Additional values are considered to be added in the future. Clients should react to an unknown operator by assuming the requirement is not satisfied.  
     * @param values {Array.<Number>} Specifies the set of values. Each returned container exit code (might be multiple in case of multiple containers) is checked against this set of values with respect to the operator. The list of values must be ordered and must not contain duplicates. Value '0' cannot be used for the In operator. At least one element is required. At most 255 elements are allowed.
     */
    constructor(operator, values) { 
        
        IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement.initialize(this, operator, values);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, operator, values) { 
        obj['operator'] = operator;
        obj['values'] = values;
    }

    /**
     * Constructs a <code>IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement} obj Optional instance to populate.
     * @return {module:model/IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement} The populated <code>IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement();

            if (data.hasOwnProperty('containerName')) {
                obj['containerName'] = ApiClient.convertToType(data['containerName'], 'String');
            }
            if (data.hasOwnProperty('operator')) {
                obj['operator'] = ApiClient.convertToType(data['operator'], 'String');
            }
            if (data.hasOwnProperty('values')) {
                obj['values'] = ApiClient.convertToType(data['values'], ['Number']);
            }
        }
        return obj;
    }


}

/**
 * Restricts the check for exit codes to the container with the specified name. When null, the rule applies to all containers. When specified, it should match one the container or initContainer names in the pod template.
 * @member {String} containerName
 */
IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement.prototype['containerName'] = undefined;

/**
 * Represents the relationship between the container exit code(s) and the specified values. Containers completed with success (exit code 0) are excluded from the requirement check. Possible values are: - In: the requirement is satisfied if at least one container exit code   (might be multiple if there are multiple containers not restricted   by the 'containerName' field) is in the set of specified values. - NotIn: the requirement is satisfied if at least one container exit code   (might be multiple if there are multiple containers not restricted   by the 'containerName' field) is not in the set of specified values. Additional values are considered to be added in the future. Clients should react to an unknown operator by assuming the requirement is not satisfied.  
 * @member {String} operator
 */
IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement.prototype['operator'] = undefined;

/**
 * Specifies the set of values. Each returned container exit code (might be multiple in case of multiple containers) is checked against this set of values with respect to the operator. The list of values must be ordered and must not contain duplicates. Value '0' cannot be used for the In operator. At least one element is required. At most 255 elements are allowed.
 * @member {Array.<Number>} values
 */
IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement.prototype['values'] = undefined;






export default IoK8sApiBatchV1PodFailurePolicyOnExitCodesRequirement;

