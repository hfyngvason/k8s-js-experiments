/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiAutoscalingV2MetricIdentifier from './IoK8sApiAutoscalingV2MetricIdentifier';
import IoK8sApiAutoscalingV2MetricValueStatus from './IoK8sApiAutoscalingV2MetricValueStatus';

/**
 * The IoK8sApiAutoscalingV2ExternalMetricStatus model module.
 * @module model/IoK8sApiAutoscalingV2ExternalMetricStatus
 * @version unversioned
 */
class IoK8sApiAutoscalingV2ExternalMetricStatus {
    /**
     * Constructs a new <code>IoK8sApiAutoscalingV2ExternalMetricStatus</code>.
     * ExternalMetricStatus indicates the current value of a global metric not associated with any Kubernetes object.
     * @alias module:model/IoK8sApiAutoscalingV2ExternalMetricStatus
     * @param current {module:model/IoK8sApiAutoscalingV2MetricValueStatus} 
     * @param metric {module:model/IoK8sApiAutoscalingV2MetricIdentifier} 
     */
    constructor(current, metric) { 
        
        IoK8sApiAutoscalingV2ExternalMetricStatus.initialize(this, current, metric);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, current, metric) { 
        obj['current'] = current;
        obj['metric'] = metric;
    }

    /**
     * Constructs a <code>IoK8sApiAutoscalingV2ExternalMetricStatus</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiAutoscalingV2ExternalMetricStatus} obj Optional instance to populate.
     * @return {module:model/IoK8sApiAutoscalingV2ExternalMetricStatus} The populated <code>IoK8sApiAutoscalingV2ExternalMetricStatus</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiAutoscalingV2ExternalMetricStatus();

            if (data.hasOwnProperty('current')) {
                obj['current'] = IoK8sApiAutoscalingV2MetricValueStatus.constructFromObject(data['current']);
            }
            if (data.hasOwnProperty('metric')) {
                obj['metric'] = IoK8sApiAutoscalingV2MetricIdentifier.constructFromObject(data['metric']);
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/IoK8sApiAutoscalingV2MetricValueStatus} current
 */
IoK8sApiAutoscalingV2ExternalMetricStatus.prototype['current'] = undefined;

/**
 * @member {module:model/IoK8sApiAutoscalingV2MetricIdentifier} metric
 */
IoK8sApiAutoscalingV2ExternalMetricStatus.prototype['metric'] = undefined;






export default IoK8sApiAutoscalingV2ExternalMetricStatus;

