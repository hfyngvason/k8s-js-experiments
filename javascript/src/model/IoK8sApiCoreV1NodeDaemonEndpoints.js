/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiCoreV1DaemonEndpoint from './IoK8sApiCoreV1DaemonEndpoint';

/**
 * The IoK8sApiCoreV1NodeDaemonEndpoints model module.
 * @module model/IoK8sApiCoreV1NodeDaemonEndpoints
 * @version unversioned
 */
class IoK8sApiCoreV1NodeDaemonEndpoints {
    /**
     * Constructs a new <code>IoK8sApiCoreV1NodeDaemonEndpoints</code>.
     * NodeDaemonEndpoints lists ports opened by daemons running on the Node.
     * @alias module:model/IoK8sApiCoreV1NodeDaemonEndpoints
     */
    constructor() { 
        
        IoK8sApiCoreV1NodeDaemonEndpoints.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiCoreV1NodeDaemonEndpoints</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiCoreV1NodeDaemonEndpoints} obj Optional instance to populate.
     * @return {module:model/IoK8sApiCoreV1NodeDaemonEndpoints} The populated <code>IoK8sApiCoreV1NodeDaemonEndpoints</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiCoreV1NodeDaemonEndpoints();

            if (data.hasOwnProperty('kubeletEndpoint')) {
                obj['kubeletEndpoint'] = IoK8sApiCoreV1DaemonEndpoint.constructFromObject(data['kubeletEndpoint']);
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/IoK8sApiCoreV1DaemonEndpoint} kubeletEndpoint
 */
IoK8sApiCoreV1NodeDaemonEndpoints.prototype['kubeletEndpoint'] = undefined;






export default IoK8sApiCoreV1NodeDaemonEndpoints;

