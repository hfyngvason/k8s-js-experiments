/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiCoreV1PersistentVolumeClaimVolumeSource model module.
 * @module model/IoK8sApiCoreV1PersistentVolumeClaimVolumeSource
 * @version unversioned
 */
class IoK8sApiCoreV1PersistentVolumeClaimVolumeSource {
    /**
     * Constructs a new <code>IoK8sApiCoreV1PersistentVolumeClaimVolumeSource</code>.
     * PersistentVolumeClaimVolumeSource references the user&#39;s PVC in the same namespace. This volume finds the bound PV and mounts that volume for the pod. A PersistentVolumeClaimVolumeSource is, essentially, a wrapper around another type of volume that is owned by someone else (the system).
     * @alias module:model/IoK8sApiCoreV1PersistentVolumeClaimVolumeSource
     * @param claimName {String} claimName is the name of a PersistentVolumeClaim in the same namespace as the pod using this volume. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#persistentvolumeclaims
     */
    constructor(claimName) { 
        
        IoK8sApiCoreV1PersistentVolumeClaimVolumeSource.initialize(this, claimName);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, claimName) { 
        obj['claimName'] = claimName;
    }

    /**
     * Constructs a <code>IoK8sApiCoreV1PersistentVolumeClaimVolumeSource</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiCoreV1PersistentVolumeClaimVolumeSource} obj Optional instance to populate.
     * @return {module:model/IoK8sApiCoreV1PersistentVolumeClaimVolumeSource} The populated <code>IoK8sApiCoreV1PersistentVolumeClaimVolumeSource</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiCoreV1PersistentVolumeClaimVolumeSource();

            if (data.hasOwnProperty('claimName')) {
                obj['claimName'] = ApiClient.convertToType(data['claimName'], 'String');
            }
            if (data.hasOwnProperty('readOnly')) {
                obj['readOnly'] = ApiClient.convertToType(data['readOnly'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * claimName is the name of a PersistentVolumeClaim in the same namespace as the pod using this volume. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#persistentvolumeclaims
 * @member {String} claimName
 */
IoK8sApiCoreV1PersistentVolumeClaimVolumeSource.prototype['claimName'] = undefined;

/**
 * readOnly Will force the ReadOnly setting in VolumeMounts. Default false.
 * @member {Boolean} readOnly
 */
IoK8sApiCoreV1PersistentVolumeClaimVolumeSource.prototype['readOnly'] = undefined;






export default IoK8sApiCoreV1PersistentVolumeClaimVolumeSource;

