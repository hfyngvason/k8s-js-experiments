/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiCoreV1PortworxVolumeSource model module.
 * @module model/IoK8sApiCoreV1PortworxVolumeSource
 * @version unversioned
 */
class IoK8sApiCoreV1PortworxVolumeSource {
    /**
     * Constructs a new <code>IoK8sApiCoreV1PortworxVolumeSource</code>.
     * PortworxVolumeSource represents a Portworx volume resource.
     * @alias module:model/IoK8sApiCoreV1PortworxVolumeSource
     * @param volumeID {String} volumeID uniquely identifies a Portworx volume
     */
    constructor(volumeID) { 
        
        IoK8sApiCoreV1PortworxVolumeSource.initialize(this, volumeID);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, volumeID) { 
        obj['volumeID'] = volumeID;
    }

    /**
     * Constructs a <code>IoK8sApiCoreV1PortworxVolumeSource</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiCoreV1PortworxVolumeSource} obj Optional instance to populate.
     * @return {module:model/IoK8sApiCoreV1PortworxVolumeSource} The populated <code>IoK8sApiCoreV1PortworxVolumeSource</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiCoreV1PortworxVolumeSource();

            if (data.hasOwnProperty('fsType')) {
                obj['fsType'] = ApiClient.convertToType(data['fsType'], 'String');
            }
            if (data.hasOwnProperty('readOnly')) {
                obj['readOnly'] = ApiClient.convertToType(data['readOnly'], 'Boolean');
            }
            if (data.hasOwnProperty('volumeID')) {
                obj['volumeID'] = ApiClient.convertToType(data['volumeID'], 'String');
            }
        }
        return obj;
    }


}

/**
 * fSType represents the filesystem type to mount Must be a filesystem type supported by the host operating system. Ex. \"ext4\", \"xfs\". Implicitly inferred to be \"ext4\" if unspecified.
 * @member {String} fsType
 */
IoK8sApiCoreV1PortworxVolumeSource.prototype['fsType'] = undefined;

/**
 * readOnly defaults to false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts.
 * @member {Boolean} readOnly
 */
IoK8sApiCoreV1PortworxVolumeSource.prototype['readOnly'] = undefined;

/**
 * volumeID uniquely identifies a Portworx volume
 * @member {String} volumeID
 */
IoK8sApiCoreV1PortworxVolumeSource.prototype['volumeID'] = undefined;






export default IoK8sApiCoreV1PortworxVolumeSource;

