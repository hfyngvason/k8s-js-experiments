/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApimachineryPkgApisMetaV1APIResource from './IoK8sApimachineryPkgApisMetaV1APIResource';

/**
 * The IoK8sApimachineryPkgApisMetaV1APIResourceList model module.
 * @module model/IoK8sApimachineryPkgApisMetaV1APIResourceList
 * @version unversioned
 */
class IoK8sApimachineryPkgApisMetaV1APIResourceList {
    /**
     * Constructs a new <code>IoK8sApimachineryPkgApisMetaV1APIResourceList</code>.
     * APIResourceList is a list of APIResource, it is used to expose the name of the resources supported in a specific group and version, and if the resource is namespaced.
     * @alias module:model/IoK8sApimachineryPkgApisMetaV1APIResourceList
     * @param groupVersion {String} groupVersion is the group and version this APIResourceList is for.
     * @param resources {Array.<module:model/IoK8sApimachineryPkgApisMetaV1APIResource>} resources contains the name of the resources and if they are namespaced.
     */
    constructor(groupVersion, resources) { 
        
        IoK8sApimachineryPkgApisMetaV1APIResourceList.initialize(this, groupVersion, resources);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, groupVersion, resources) { 
        obj['groupVersion'] = groupVersion;
        obj['resources'] = resources;
    }

    /**
     * Constructs a <code>IoK8sApimachineryPkgApisMetaV1APIResourceList</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApimachineryPkgApisMetaV1APIResourceList} obj Optional instance to populate.
     * @return {module:model/IoK8sApimachineryPkgApisMetaV1APIResourceList} The populated <code>IoK8sApimachineryPkgApisMetaV1APIResourceList</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApimachineryPkgApisMetaV1APIResourceList();

            if (data.hasOwnProperty('apiVersion')) {
                obj['apiVersion'] = ApiClient.convertToType(data['apiVersion'], 'String');
            }
            if (data.hasOwnProperty('groupVersion')) {
                obj['groupVersion'] = ApiClient.convertToType(data['groupVersion'], 'String');
            }
            if (data.hasOwnProperty('kind')) {
                obj['kind'] = ApiClient.convertToType(data['kind'], 'String');
            }
            if (data.hasOwnProperty('resources')) {
                obj['resources'] = ApiClient.convertToType(data['resources'], [IoK8sApimachineryPkgApisMetaV1APIResource]);
            }
        }
        return obj;
    }


}

/**
 * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources
 * @member {String} apiVersion
 */
IoK8sApimachineryPkgApisMetaV1APIResourceList.prototype['apiVersion'] = undefined;

/**
 * groupVersion is the group and version this APIResourceList is for.
 * @member {String} groupVersion
 */
IoK8sApimachineryPkgApisMetaV1APIResourceList.prototype['groupVersion'] = undefined;

/**
 * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
 * @member {String} kind
 */
IoK8sApimachineryPkgApisMetaV1APIResourceList.prototype['kind'] = undefined;

/**
 * resources contains the name of the resources and if they are namespaced.
 * @member {Array.<module:model/IoK8sApimachineryPkgApisMetaV1APIResource>} resources
 */
IoK8sApimachineryPkgApisMetaV1APIResourceList.prototype['resources'] = undefined;






export default IoK8sApimachineryPkgApisMetaV1APIResourceList;

