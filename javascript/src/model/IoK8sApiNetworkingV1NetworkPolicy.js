/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import IoK8sApiNetworkingV1NetworkPolicySpec from './IoK8sApiNetworkingV1NetworkPolicySpec';
import IoK8sApiNetworkingV1NetworkPolicyStatus from './IoK8sApiNetworkingV1NetworkPolicyStatus';
import IoK8sApimachineryPkgApisMetaV1ObjectMeta from './IoK8sApimachineryPkgApisMetaV1ObjectMeta';

/**
 * The IoK8sApiNetworkingV1NetworkPolicy model module.
 * @module model/IoK8sApiNetworkingV1NetworkPolicy
 * @version unversioned
 */
class IoK8sApiNetworkingV1NetworkPolicy {
    /**
     * Constructs a new <code>IoK8sApiNetworkingV1NetworkPolicy</code>.
     * NetworkPolicy describes what network traffic is allowed for a set of Pods
     * @alias module:model/IoK8sApiNetworkingV1NetworkPolicy
     */
    constructor() { 
        
        IoK8sApiNetworkingV1NetworkPolicy.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiNetworkingV1NetworkPolicy</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiNetworkingV1NetworkPolicy} obj Optional instance to populate.
     * @return {module:model/IoK8sApiNetworkingV1NetworkPolicy} The populated <code>IoK8sApiNetworkingV1NetworkPolicy</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiNetworkingV1NetworkPolicy();

            if (data.hasOwnProperty('apiVersion')) {
                obj['apiVersion'] = ApiClient.convertToType(data['apiVersion'], 'String');
            }
            if (data.hasOwnProperty('kind')) {
                obj['kind'] = ApiClient.convertToType(data['kind'], 'String');
            }
            if (data.hasOwnProperty('metadata')) {
                obj['metadata'] = IoK8sApimachineryPkgApisMetaV1ObjectMeta.constructFromObject(data['metadata']);
            }
            if (data.hasOwnProperty('spec')) {
                obj['spec'] = IoK8sApiNetworkingV1NetworkPolicySpec.constructFromObject(data['spec']);
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = IoK8sApiNetworkingV1NetworkPolicyStatus.constructFromObject(data['status']);
            }
        }
        return obj;
    }


}

/**
 * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources
 * @member {String} apiVersion
 */
IoK8sApiNetworkingV1NetworkPolicy.prototype['apiVersion'] = undefined;

/**
 * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
 * @member {String} kind
 */
IoK8sApiNetworkingV1NetworkPolicy.prototype['kind'] = undefined;

/**
 * @member {module:model/IoK8sApimachineryPkgApisMetaV1ObjectMeta} metadata
 */
IoK8sApiNetworkingV1NetworkPolicy.prototype['metadata'] = undefined;

/**
 * @member {module:model/IoK8sApiNetworkingV1NetworkPolicySpec} spec
 */
IoK8sApiNetworkingV1NetworkPolicy.prototype['spec'] = undefined;

/**
 * @member {module:model/IoK8sApiNetworkingV1NetworkPolicyStatus} status
 */
IoK8sApiNetworkingV1NetworkPolicy.prototype['status'] = undefined;






export default IoK8sApiNetworkingV1NetworkPolicy;

