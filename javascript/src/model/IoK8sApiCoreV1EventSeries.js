/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiCoreV1EventSeries model module.
 * @module model/IoK8sApiCoreV1EventSeries
 * @version unversioned
 */
class IoK8sApiCoreV1EventSeries {
    /**
     * Constructs a new <code>IoK8sApiCoreV1EventSeries</code>.
     * EventSeries contain information on series of events, i.e. thing that was/is happening continuously for some time.
     * @alias module:model/IoK8sApiCoreV1EventSeries
     */
    constructor() { 
        
        IoK8sApiCoreV1EventSeries.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IoK8sApiCoreV1EventSeries</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiCoreV1EventSeries} obj Optional instance to populate.
     * @return {module:model/IoK8sApiCoreV1EventSeries} The populated <code>IoK8sApiCoreV1EventSeries</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiCoreV1EventSeries();

            if (data.hasOwnProperty('count')) {
                obj['count'] = ApiClient.convertToType(data['count'], 'Number');
            }
            if (data.hasOwnProperty('lastObservedTime')) {
                obj['lastObservedTime'] = ApiClient.convertToType(data['lastObservedTime'], 'Date');
            }
        }
        return obj;
    }


}

/**
 * Number of occurrences in this series up to the last heartbeat time
 * @member {Number} count
 */
IoK8sApiCoreV1EventSeries.prototype['count'] = undefined;

/**
 * MicroTime is version of Time with microsecond level precision.
 * @member {Date} lastObservedTime
 */
IoK8sApiCoreV1EventSeries.prototype['lastObservedTime'] = undefined;






export default IoK8sApiCoreV1EventSeries;

