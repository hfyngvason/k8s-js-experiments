/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames model module.
 * @module model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames
 * @version unversioned
 */
class IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames {
    /**
     * Constructs a new <code>IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames</code>.
     * CustomResourceDefinitionNames indicates the names to serve this CustomResourceDefinition
     * @alias module:model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames
     * @param kind {String} kind is the serialized kind of the resource. It is normally CamelCase and singular. Custom resource instances will use this value as the `kind` attribute in API calls.
     * @param plural {String} plural is the plural name of the resource to serve. The custom resources are served under `/apis/<group>/<version>/.../<plural>`. Must match the name of the CustomResourceDefinition (in the form `<names.plural>.<group>`). Must be all lowercase.
     */
    constructor(kind, plural) { 
        
        IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames.initialize(this, kind, plural);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, kind, plural) { 
        obj['kind'] = kind;
        obj['plural'] = plural;
    }

    /**
     * Constructs a <code>IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames} obj Optional instance to populate.
     * @return {module:model/IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames} The populated <code>IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames();

            if (data.hasOwnProperty('categories')) {
                obj['categories'] = ApiClient.convertToType(data['categories'], ['String']);
            }
            if (data.hasOwnProperty('kind')) {
                obj['kind'] = ApiClient.convertToType(data['kind'], 'String');
            }
            if (data.hasOwnProperty('listKind')) {
                obj['listKind'] = ApiClient.convertToType(data['listKind'], 'String');
            }
            if (data.hasOwnProperty('plural')) {
                obj['plural'] = ApiClient.convertToType(data['plural'], 'String');
            }
            if (data.hasOwnProperty('shortNames')) {
                obj['shortNames'] = ApiClient.convertToType(data['shortNames'], ['String']);
            }
            if (data.hasOwnProperty('singular')) {
                obj['singular'] = ApiClient.convertToType(data['singular'], 'String');
            }
        }
        return obj;
    }


}

/**
 * categories is a list of grouped resources this custom resource belongs to (e.g. 'all'). This is published in API discovery documents, and used by clients to support invocations like `kubectl get all`.
 * @member {Array.<String>} categories
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames.prototype['categories'] = undefined;

/**
 * kind is the serialized kind of the resource. It is normally CamelCase and singular. Custom resource instances will use this value as the `kind` attribute in API calls.
 * @member {String} kind
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames.prototype['kind'] = undefined;

/**
 * listKind is the serialized kind of the list for this resource. Defaults to \"`kind`List\".
 * @member {String} listKind
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames.prototype['listKind'] = undefined;

/**
 * plural is the plural name of the resource to serve. The custom resources are served under `/apis/<group>/<version>/.../<plural>`. Must match the name of the CustomResourceDefinition (in the form `<names.plural>.<group>`). Must be all lowercase.
 * @member {String} plural
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames.prototype['plural'] = undefined;

/**
 * shortNames are short names for the resource, exposed in API discovery documents, and used by clients to support invocations like `kubectl get <shortname>`. It must be all lowercase.
 * @member {Array.<String>} shortNames
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames.prototype['shortNames'] = undefined;

/**
 * singular is the singular name of the resource. It must be all lowercase. Defaults to lowercased `kind`.
 * @member {String} singular
 */
IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames.prototype['singular'] = undefined;






export default IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceDefinitionNames;

