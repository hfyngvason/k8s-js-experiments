/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.Kubernetes);
  }
}(this, function(expect, Kubernetes) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('IoK8sApimachineryPkgVersionInfo', function() {
    it('should create an instance of IoK8sApimachineryPkgVersionInfo', function() {
      // uncomment below and update the code to test IoK8sApimachineryPkgVersionInfo
      //var instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
      //expect(instance).to.be.a(Kubernetes.IoK8sApimachineryPkgVersionInfo);
    });

    it('should have the property buildDate (base name: "buildDate")', function() {
      // uncomment below and update the code to test the property buildDate
      //var instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
      //expect(instance).to.be();
    });

    it('should have the property compiler (base name: "compiler")', function() {
      // uncomment below and update the code to test the property compiler
      //var instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
      //expect(instance).to.be();
    });

    it('should have the property gitCommit (base name: "gitCommit")', function() {
      // uncomment below and update the code to test the property gitCommit
      //var instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
      //expect(instance).to.be();
    });

    it('should have the property gitTreeState (base name: "gitTreeState")', function() {
      // uncomment below and update the code to test the property gitTreeState
      //var instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
      //expect(instance).to.be();
    });

    it('should have the property gitVersion (base name: "gitVersion")', function() {
      // uncomment below and update the code to test the property gitVersion
      //var instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
      //expect(instance).to.be();
    });

    it('should have the property goVersion (base name: "goVersion")', function() {
      // uncomment below and update the code to test the property goVersion
      //var instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
      //expect(instance).to.be();
    });

    it('should have the property major (base name: "major")', function() {
      // uncomment below and update the code to test the property major
      //var instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
      //expect(instance).to.be();
    });

    it('should have the property minor (base name: "minor")', function() {
      // uncomment below and update the code to test the property minor
      //var instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
      //expect(instance).to.be();
    });

    it('should have the property platform (base name: "platform")', function() {
      // uncomment below and update the code to test the property platform
      //var instance = new Kubernetes.IoK8sApimachineryPkgVersionInfo();
      //expect(instance).to.be();
    });

  });

}));
