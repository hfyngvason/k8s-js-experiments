/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.Kubernetes);
  }
}(this, function(expect, Kubernetes) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new Kubernetes.IoK8sApiCoordinationV1LeaseSpec();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('IoK8sApiCoordinationV1LeaseSpec', function() {
    it('should create an instance of IoK8sApiCoordinationV1LeaseSpec', function() {
      // uncomment below and update the code to test IoK8sApiCoordinationV1LeaseSpec
      //var instance = new Kubernetes.IoK8sApiCoordinationV1LeaseSpec();
      //expect(instance).to.be.a(Kubernetes.IoK8sApiCoordinationV1LeaseSpec);
    });

    it('should have the property acquireTime (base name: "acquireTime")', function() {
      // uncomment below and update the code to test the property acquireTime
      //var instance = new Kubernetes.IoK8sApiCoordinationV1LeaseSpec();
      //expect(instance).to.be();
    });

    it('should have the property holderIdentity (base name: "holderIdentity")', function() {
      // uncomment below and update the code to test the property holderIdentity
      //var instance = new Kubernetes.IoK8sApiCoordinationV1LeaseSpec();
      //expect(instance).to.be();
    });

    it('should have the property leaseDurationSeconds (base name: "leaseDurationSeconds")', function() {
      // uncomment below and update the code to test the property leaseDurationSeconds
      //var instance = new Kubernetes.IoK8sApiCoordinationV1LeaseSpec();
      //expect(instance).to.be();
    });

    it('should have the property leaseTransitions (base name: "leaseTransitions")', function() {
      // uncomment below and update the code to test the property leaseTransitions
      //var instance = new Kubernetes.IoK8sApiCoordinationV1LeaseSpec();
      //expect(instance).to.be();
    });

    it('should have the property renewTime (base name: "renewTime")', function() {
      // uncomment below and update the code to test the property renewTime
      //var instance = new Kubernetes.IoK8sApiCoordinationV1LeaseSpec();
      //expect(instance).to.be();
    });

  });

}));
