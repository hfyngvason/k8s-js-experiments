/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.Kubernetes);
  }
}(this, function(expect, Kubernetes) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('IoK8sApiBatchV1JobSpec', function() {
    it('should create an instance of IoK8sApiBatchV1JobSpec', function() {
      // uncomment below and update the code to test IoK8sApiBatchV1JobSpec
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be.a(Kubernetes.IoK8sApiBatchV1JobSpec);
    });

    it('should have the property activeDeadlineSeconds (base name: "activeDeadlineSeconds")', function() {
      // uncomment below and update the code to test the property activeDeadlineSeconds
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

    it('should have the property backoffLimit (base name: "backoffLimit")', function() {
      // uncomment below and update the code to test the property backoffLimit
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

    it('should have the property completionMode (base name: "completionMode")', function() {
      // uncomment below and update the code to test the property completionMode
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

    it('should have the property completions (base name: "completions")', function() {
      // uncomment below and update the code to test the property completions
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

    it('should have the property manualSelector (base name: "manualSelector")', function() {
      // uncomment below and update the code to test the property manualSelector
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

    it('should have the property parallelism (base name: "parallelism")', function() {
      // uncomment below and update the code to test the property parallelism
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

    it('should have the property podFailurePolicy (base name: "podFailurePolicy")', function() {
      // uncomment below and update the code to test the property podFailurePolicy
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

    it('should have the property selector (base name: "selector")', function() {
      // uncomment below and update the code to test the property selector
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

    it('should have the property suspend (base name: "suspend")', function() {
      // uncomment below and update the code to test the property suspend
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

    it('should have the property template (base name: "template")', function() {
      // uncomment below and update the code to test the property template
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

    it('should have the property ttlSecondsAfterFinished (base name: "ttlSecondsAfterFinished")', function() {
      // uncomment below and update the code to test the property ttlSecondsAfterFinished
      //var instance = new Kubernetes.IoK8sApiBatchV1JobSpec();
      //expect(instance).to.be();
    });

  });

}));
