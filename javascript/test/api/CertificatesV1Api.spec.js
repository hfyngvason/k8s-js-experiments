/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.Kubernetes);
  }
}(this, function(expect, Kubernetes) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new Kubernetes.CertificatesV1Api();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('CertificatesV1Api', function() {
    describe('createCertificatesV1CertificateSigningRequest', function() {
      it('should call createCertificatesV1CertificateSigningRequest successfully', function(done) {
        //uncomment below and update the code to test createCertificatesV1CertificateSigningRequest
        //instance.createCertificatesV1CertificateSigningRequest(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteCertificatesV1CertificateSigningRequest', function() {
      it('should call deleteCertificatesV1CertificateSigningRequest successfully', function(done) {
        //uncomment below and update the code to test deleteCertificatesV1CertificateSigningRequest
        //instance.deleteCertificatesV1CertificateSigningRequest(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteCertificatesV1CollectionCertificateSigningRequest', function() {
      it('should call deleteCertificatesV1CollectionCertificateSigningRequest successfully', function(done) {
        //uncomment below and update the code to test deleteCertificatesV1CollectionCertificateSigningRequest
        //instance.deleteCertificatesV1CollectionCertificateSigningRequest(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getCertificatesV1APIResources', function() {
      it('should call getCertificatesV1APIResources successfully', function(done) {
        //uncomment below and update the code to test getCertificatesV1APIResources
        //instance.getCertificatesV1APIResources(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('listCertificatesV1CertificateSigningRequest', function() {
      it('should call listCertificatesV1CertificateSigningRequest successfully', function(done) {
        //uncomment below and update the code to test listCertificatesV1CertificateSigningRequest
        //instance.listCertificatesV1CertificateSigningRequest(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('patchCertificatesV1CertificateSigningRequest', function() {
      it('should call patchCertificatesV1CertificateSigningRequest successfully', function(done) {
        //uncomment below and update the code to test patchCertificatesV1CertificateSigningRequest
        //instance.patchCertificatesV1CertificateSigningRequest(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('patchCertificatesV1CertificateSigningRequestApproval', function() {
      it('should call patchCertificatesV1CertificateSigningRequestApproval successfully', function(done) {
        //uncomment below and update the code to test patchCertificatesV1CertificateSigningRequestApproval
        //instance.patchCertificatesV1CertificateSigningRequestApproval(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('patchCertificatesV1CertificateSigningRequestStatus', function() {
      it('should call patchCertificatesV1CertificateSigningRequestStatus successfully', function(done) {
        //uncomment below and update the code to test patchCertificatesV1CertificateSigningRequestStatus
        //instance.patchCertificatesV1CertificateSigningRequestStatus(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('readCertificatesV1CertificateSigningRequest', function() {
      it('should call readCertificatesV1CertificateSigningRequest successfully', function(done) {
        //uncomment below and update the code to test readCertificatesV1CertificateSigningRequest
        //instance.readCertificatesV1CertificateSigningRequest(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('readCertificatesV1CertificateSigningRequestApproval', function() {
      it('should call readCertificatesV1CertificateSigningRequestApproval successfully', function(done) {
        //uncomment below and update the code to test readCertificatesV1CertificateSigningRequestApproval
        //instance.readCertificatesV1CertificateSigningRequestApproval(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('readCertificatesV1CertificateSigningRequestStatus', function() {
      it('should call readCertificatesV1CertificateSigningRequestStatus successfully', function(done) {
        //uncomment below and update the code to test readCertificatesV1CertificateSigningRequestStatus
        //instance.readCertificatesV1CertificateSigningRequestStatus(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('replaceCertificatesV1CertificateSigningRequest', function() {
      it('should call replaceCertificatesV1CertificateSigningRequest successfully', function(done) {
        //uncomment below and update the code to test replaceCertificatesV1CertificateSigningRequest
        //instance.replaceCertificatesV1CertificateSigningRequest(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('replaceCertificatesV1CertificateSigningRequestApproval', function() {
      it('should call replaceCertificatesV1CertificateSigningRequestApproval successfully', function(done) {
        //uncomment below and update the code to test replaceCertificatesV1CertificateSigningRequestApproval
        //instance.replaceCertificatesV1CertificateSigningRequestApproval(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('replaceCertificatesV1CertificateSigningRequestStatus', function() {
      it('should call replaceCertificatesV1CertificateSigningRequestStatus successfully', function(done) {
        //uncomment below and update the code to test replaceCertificatesV1CertificateSigningRequestStatus
        //instance.replaceCertificatesV1CertificateSigningRequestStatus(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('watchCertificatesV1CertificateSigningRequest', function() {
      it('should call watchCertificatesV1CertificateSigningRequest successfully', function(done) {
        //uncomment below and update the code to test watchCertificatesV1CertificateSigningRequest
        //instance.watchCertificatesV1CertificateSigningRequest(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('watchCertificatesV1CertificateSigningRequestList', function() {
      it('should call watchCertificatesV1CertificateSigningRequestList successfully', function(done) {
        //uncomment below and update the code to test watchCertificatesV1CertificateSigningRequestList
        //instance.watchCertificatesV1CertificateSigningRequestList(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
