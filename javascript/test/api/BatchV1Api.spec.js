/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.Kubernetes);
  }
}(this, function(expect, Kubernetes) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new Kubernetes.BatchV1Api();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('BatchV1Api', function() {
    describe('createBatchV1NamespacedCronJob', function() {
      it('should call createBatchV1NamespacedCronJob successfully', function(done) {
        //uncomment below and update the code to test createBatchV1NamespacedCronJob
        //instance.createBatchV1NamespacedCronJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('createBatchV1NamespacedJob', function() {
      it('should call createBatchV1NamespacedJob successfully', function(done) {
        //uncomment below and update the code to test createBatchV1NamespacedJob
        //instance.createBatchV1NamespacedJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteBatchV1CollectionNamespacedCronJob', function() {
      it('should call deleteBatchV1CollectionNamespacedCronJob successfully', function(done) {
        //uncomment below and update the code to test deleteBatchV1CollectionNamespacedCronJob
        //instance.deleteBatchV1CollectionNamespacedCronJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteBatchV1CollectionNamespacedJob', function() {
      it('should call deleteBatchV1CollectionNamespacedJob successfully', function(done) {
        //uncomment below and update the code to test deleteBatchV1CollectionNamespacedJob
        //instance.deleteBatchV1CollectionNamespacedJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteBatchV1NamespacedCronJob', function() {
      it('should call deleteBatchV1NamespacedCronJob successfully', function(done) {
        //uncomment below and update the code to test deleteBatchV1NamespacedCronJob
        //instance.deleteBatchV1NamespacedCronJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteBatchV1NamespacedJob', function() {
      it('should call deleteBatchV1NamespacedJob successfully', function(done) {
        //uncomment below and update the code to test deleteBatchV1NamespacedJob
        //instance.deleteBatchV1NamespacedJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getBatchV1APIResources', function() {
      it('should call getBatchV1APIResources successfully', function(done) {
        //uncomment below and update the code to test getBatchV1APIResources
        //instance.getBatchV1APIResources(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('listBatchV1CronJobForAllNamespaces', function() {
      it('should call listBatchV1CronJobForAllNamespaces successfully', function(done) {
        //uncomment below and update the code to test listBatchV1CronJobForAllNamespaces
        //instance.listBatchV1CronJobForAllNamespaces(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('listBatchV1JobForAllNamespaces', function() {
      it('should call listBatchV1JobForAllNamespaces successfully', function(done) {
        //uncomment below and update the code to test listBatchV1JobForAllNamespaces
        //instance.listBatchV1JobForAllNamespaces(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('listBatchV1NamespacedCronJob', function() {
      it('should call listBatchV1NamespacedCronJob successfully', function(done) {
        //uncomment below and update the code to test listBatchV1NamespacedCronJob
        //instance.listBatchV1NamespacedCronJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('listBatchV1NamespacedJob', function() {
      it('should call listBatchV1NamespacedJob successfully', function(done) {
        //uncomment below and update the code to test listBatchV1NamespacedJob
        //instance.listBatchV1NamespacedJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('patchBatchV1NamespacedCronJob', function() {
      it('should call patchBatchV1NamespacedCronJob successfully', function(done) {
        //uncomment below and update the code to test patchBatchV1NamespacedCronJob
        //instance.patchBatchV1NamespacedCronJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('patchBatchV1NamespacedCronJobStatus', function() {
      it('should call patchBatchV1NamespacedCronJobStatus successfully', function(done) {
        //uncomment below and update the code to test patchBatchV1NamespacedCronJobStatus
        //instance.patchBatchV1NamespacedCronJobStatus(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('patchBatchV1NamespacedJob', function() {
      it('should call patchBatchV1NamespacedJob successfully', function(done) {
        //uncomment below and update the code to test patchBatchV1NamespacedJob
        //instance.patchBatchV1NamespacedJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('patchBatchV1NamespacedJobStatus', function() {
      it('should call patchBatchV1NamespacedJobStatus successfully', function(done) {
        //uncomment below and update the code to test patchBatchV1NamespacedJobStatus
        //instance.patchBatchV1NamespacedJobStatus(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('readBatchV1NamespacedCronJob', function() {
      it('should call readBatchV1NamespacedCronJob successfully', function(done) {
        //uncomment below and update the code to test readBatchV1NamespacedCronJob
        //instance.readBatchV1NamespacedCronJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('readBatchV1NamespacedCronJobStatus', function() {
      it('should call readBatchV1NamespacedCronJobStatus successfully', function(done) {
        //uncomment below and update the code to test readBatchV1NamespacedCronJobStatus
        //instance.readBatchV1NamespacedCronJobStatus(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('readBatchV1NamespacedJob', function() {
      it('should call readBatchV1NamespacedJob successfully', function(done) {
        //uncomment below and update the code to test readBatchV1NamespacedJob
        //instance.readBatchV1NamespacedJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('readBatchV1NamespacedJobStatus', function() {
      it('should call readBatchV1NamespacedJobStatus successfully', function(done) {
        //uncomment below and update the code to test readBatchV1NamespacedJobStatus
        //instance.readBatchV1NamespacedJobStatus(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('replaceBatchV1NamespacedCronJob', function() {
      it('should call replaceBatchV1NamespacedCronJob successfully', function(done) {
        //uncomment below and update the code to test replaceBatchV1NamespacedCronJob
        //instance.replaceBatchV1NamespacedCronJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('replaceBatchV1NamespacedCronJobStatus', function() {
      it('should call replaceBatchV1NamespacedCronJobStatus successfully', function(done) {
        //uncomment below and update the code to test replaceBatchV1NamespacedCronJobStatus
        //instance.replaceBatchV1NamespacedCronJobStatus(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('replaceBatchV1NamespacedJob', function() {
      it('should call replaceBatchV1NamespacedJob successfully', function(done) {
        //uncomment below and update the code to test replaceBatchV1NamespacedJob
        //instance.replaceBatchV1NamespacedJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('replaceBatchV1NamespacedJobStatus', function() {
      it('should call replaceBatchV1NamespacedJobStatus successfully', function(done) {
        //uncomment below and update the code to test replaceBatchV1NamespacedJobStatus
        //instance.replaceBatchV1NamespacedJobStatus(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('watchBatchV1CronJobListForAllNamespaces', function() {
      it('should call watchBatchV1CronJobListForAllNamespaces successfully', function(done) {
        //uncomment below and update the code to test watchBatchV1CronJobListForAllNamespaces
        //instance.watchBatchV1CronJobListForAllNamespaces(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('watchBatchV1JobListForAllNamespaces', function() {
      it('should call watchBatchV1JobListForAllNamespaces successfully', function(done) {
        //uncomment below and update the code to test watchBatchV1JobListForAllNamespaces
        //instance.watchBatchV1JobListForAllNamespaces(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('watchBatchV1NamespacedCronJob', function() {
      it('should call watchBatchV1NamespacedCronJob successfully', function(done) {
        //uncomment below and update the code to test watchBatchV1NamespacedCronJob
        //instance.watchBatchV1NamespacedCronJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('watchBatchV1NamespacedCronJobList', function() {
      it('should call watchBatchV1NamespacedCronJobList successfully', function(done) {
        //uncomment below and update the code to test watchBatchV1NamespacedCronJobList
        //instance.watchBatchV1NamespacedCronJobList(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('watchBatchV1NamespacedJob', function() {
      it('should call watchBatchV1NamespacedJob successfully', function(done) {
        //uncomment below and update the code to test watchBatchV1NamespacedJob
        //instance.watchBatchV1NamespacedJob(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('watchBatchV1NamespacedJobList', function() {
      it('should call watchBatchV1NamespacedJobList successfully', function(done) {
        //uncomment below and update the code to test watchBatchV1NamespacedJobList
        //instance.watchBatchV1NamespacedJobList(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
