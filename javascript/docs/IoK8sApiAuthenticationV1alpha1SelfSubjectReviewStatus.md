# Kubernetes.IoK8sApiAuthenticationV1alpha1SelfSubjectReviewStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userInfo** | [**IoK8sApiAuthenticationV1UserInfo**](IoK8sApiAuthenticationV1UserInfo.md) |  | [optional] 


