# Kubernetes.IoK8sApiFlowcontrolV1beta3LimitedPriorityLevelConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**limitResponse** | [**IoK8sApiFlowcontrolV1beta3LimitResponse**](IoK8sApiFlowcontrolV1beta3LimitResponse.md) |  | [optional] 
**nominalConcurrencyShares** | **Number** | &#x60;nominalConcurrencyShares&#x60; (NCS) contributes to the computation of the NominalConcurrencyLimit (NominalCL) of this level. This is the number of execution seats available at this priority level. This is used both for requests dispatched from this priority level as well as requests dispatched from other priority levels borrowing seats from this level. The server&#39;s concurrency limit (ServerCL) is divided among the Limited priority levels in proportion to their NCS values:  NominalCL(i)  &#x3D; ceil( ServerCL * NCS(i) / sum_ncs ) sum_ncs &#x3D; sum[limited priority level k] NCS(k)  Bigger numbers mean a larger nominal concurrency limit, at the expense of every other Limited priority level. This field has a default value of 30. | [optional] 


