# Kubernetes.IoK8sApiCoreV1DownwardAPIProjection

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**[IoK8sApiCoreV1DownwardAPIVolumeFile]**](IoK8sApiCoreV1DownwardAPIVolumeFile.md) | Items is a list of DownwardAPIVolume file | [optional] 


