# Kubernetes.IoK8sApiBatchV1CronJobStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | [**[IoK8sApiCoreV1ObjectReference]**](IoK8sApiCoreV1ObjectReference.md) | A list of pointers to currently running jobs. | [optional] 
**lastScheduleTime** | **Date** | Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers. | [optional] 
**lastSuccessfulTime** | **Date** | Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers. | [optional] 


