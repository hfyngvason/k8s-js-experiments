# Kubernetes.IoK8sApiCoreV1TopologySelectorTerm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**matchLabelExpressions** | [**[IoK8sApiCoreV1TopologySelectorLabelRequirement]**](IoK8sApiCoreV1TopologySelectorLabelRequirement.md) | A list of topology selector requirements by labels. | [optional] 


