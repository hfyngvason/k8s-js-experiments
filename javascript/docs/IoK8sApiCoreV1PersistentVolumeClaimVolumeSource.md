# Kubernetes.IoK8sApiCoreV1PersistentVolumeClaimVolumeSource

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**claimName** | **String** | claimName is the name of a PersistentVolumeClaim in the same namespace as the pod using this volume. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#persistentvolumeclaims | 
**readOnly** | **Boolean** | readOnly Will force the ReadOnly setting in VolumeMounts. Default false. | [optional] 


