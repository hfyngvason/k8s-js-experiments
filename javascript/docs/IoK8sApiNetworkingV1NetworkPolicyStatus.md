# Kubernetes.IoK8sApiNetworkingV1NetworkPolicyStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conditions** | [**[IoK8sApimachineryPkgApisMetaV1Condition]**](IoK8sApimachineryPkgApisMetaV1Condition.md) | Conditions holds an array of metav1.Condition that describe the state of the NetworkPolicy. Current service state | [optional] 


