# Kubernetes.IoK8sApiDiscoveryV1EndpointHints

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forZones** | [**[IoK8sApiDiscoveryV1ForZone]**](IoK8sApiDiscoveryV1ForZone.md) | forZones indicates the zone(s) this endpoint should be consumed by to enable topology aware routing. | [optional] 


