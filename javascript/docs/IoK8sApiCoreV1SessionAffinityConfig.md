# Kubernetes.IoK8sApiCoreV1SessionAffinityConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientIP** | [**IoK8sApiCoreV1ClientIPConfig**](IoK8sApiCoreV1ClientIPConfig.md) |  | [optional] 


