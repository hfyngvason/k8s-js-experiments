# Kubernetes.IoK8sApiCoreV1NodeSelectorTerm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**matchExpressions** | [**[IoK8sApiCoreV1NodeSelectorRequirement]**](IoK8sApiCoreV1NodeSelectorRequirement.md) | A list of node selector requirements by node&#39;s labels. | [optional] 
**matchFields** | [**[IoK8sApiCoreV1NodeSelectorRequirement]**](IoK8sApiCoreV1NodeSelectorRequirement.md) | A list of node selector requirements by node&#39;s fields. | [optional] 


