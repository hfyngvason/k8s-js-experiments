# Kubernetes.IoK8sApiCoreV1LimitRangeSpec

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**limits** | [**[IoK8sApiCoreV1LimitRangeItem]**](IoK8sApiCoreV1LimitRangeItem.md) | Limits is the list of LimitRangeItem objects that are enforced. | 


