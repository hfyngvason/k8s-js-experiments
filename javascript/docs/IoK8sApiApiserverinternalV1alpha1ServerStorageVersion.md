# Kubernetes.IoK8sApiApiserverinternalV1alpha1ServerStorageVersion

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiServerID** | **String** | The ID of the reporting API server. | [optional] 
**decodableVersions** | **[String]** | The API server can decode objects encoded in these versions. The encodingVersion must be included in the decodableVersions. | [optional] 
**encodingVersion** | **String** | The API server encodes the object to this version when persisting it in the backend (e.g., etcd). | [optional] 


