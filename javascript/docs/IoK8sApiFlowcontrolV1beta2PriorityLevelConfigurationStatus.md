# Kubernetes.IoK8sApiFlowcontrolV1beta2PriorityLevelConfigurationStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conditions** | [**[IoK8sApiFlowcontrolV1beta2PriorityLevelConfigurationCondition]**](IoK8sApiFlowcontrolV1beta2PriorityLevelConfigurationCondition.md) | &#x60;conditions&#x60; is the current state of \&quot;request-priority\&quot;. | [optional] 


