# Kubernetes.IoK8sApiCoreV1NodeDaemonEndpoints

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kubeletEndpoint** | [**IoK8sApiCoreV1DaemonEndpoint**](IoK8sApiCoreV1DaemonEndpoint.md) |  | [optional] 


