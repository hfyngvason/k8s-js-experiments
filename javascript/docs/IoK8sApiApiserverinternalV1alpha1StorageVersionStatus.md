# Kubernetes.IoK8sApiApiserverinternalV1alpha1StorageVersionStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commonEncodingVersion** | **String** | If all API server instances agree on the same encoding storage version, then this field is set to that version. Otherwise this field is left empty. API servers should finish updating its storageVersionStatus entry before serving write operations, so that this field will be in sync with the reality. | [optional] 
**conditions** | [**[IoK8sApiApiserverinternalV1alpha1StorageVersionCondition]**](IoK8sApiApiserverinternalV1alpha1StorageVersionCondition.md) | The latest available observations of the storageVersion&#39;s state. | [optional] 
**storageVersions** | [**[IoK8sApiApiserverinternalV1alpha1ServerStorageVersion]**](IoK8sApiApiserverinternalV1alpha1ServerStorageVersion.md) | The reported versions per API server instance. | [optional] 


