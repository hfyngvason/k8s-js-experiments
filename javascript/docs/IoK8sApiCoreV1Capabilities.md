# Kubernetes.IoK8sApiCoreV1Capabilities

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**add** | **[String]** | Added capabilities | [optional] 
**drop** | **[String]** | Removed capabilities | [optional] 


