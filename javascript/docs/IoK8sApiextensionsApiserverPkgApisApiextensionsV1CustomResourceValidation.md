# Kubernetes.IoK8sApiextensionsApiserverPkgApisApiextensionsV1CustomResourceValidation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**openAPIV3Schema** | [**IoK8sApiextensionsApiserverPkgApisApiextensionsV1JSONSchemaProps**](IoK8sApiextensionsApiserverPkgApisApiextensionsV1JSONSchemaProps.md) |  | [optional] 


