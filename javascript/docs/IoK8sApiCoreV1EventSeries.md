# Kubernetes.IoK8sApiCoreV1EventSeries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Number** | Number of occurrences in this series up to the last heartbeat time | [optional] 
**lastObservedTime** | **Date** | MicroTime is version of Time with microsecond level precision. | [optional] 


