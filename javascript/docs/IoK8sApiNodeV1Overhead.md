# Kubernetes.IoK8sApiNodeV1Overhead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**podFixed** | **{String: String}** | PodFixed represents the fixed resource overhead associated with running a pod. | [optional] 


