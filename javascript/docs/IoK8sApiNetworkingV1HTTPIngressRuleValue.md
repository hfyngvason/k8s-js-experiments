# Kubernetes.IoK8sApiNetworkingV1HTTPIngressRuleValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paths** | [**[IoK8sApiNetworkingV1HTTPIngressPath]**](IoK8sApiNetworkingV1HTTPIngressPath.md) | A collection of paths that map requests to backends. | 


