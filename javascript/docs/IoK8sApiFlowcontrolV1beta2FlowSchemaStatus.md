# Kubernetes.IoK8sApiFlowcontrolV1beta2FlowSchemaStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conditions** | [**[IoK8sApiFlowcontrolV1beta2FlowSchemaCondition]**](IoK8sApiFlowcontrolV1beta2FlowSchemaCondition.md) | &#x60;conditions&#x60; is a list of the current states of FlowSchema. | [optional] 


