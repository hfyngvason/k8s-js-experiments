# Kubernetes.IoK8sApiAutoscalingV2ResourceMetricStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current** | [**IoK8sApiAutoscalingV2MetricValueStatus**](IoK8sApiAutoscalingV2MetricValueStatus.md) |  | 
**name** | **String** | Name is the name of the resource in question. | 


