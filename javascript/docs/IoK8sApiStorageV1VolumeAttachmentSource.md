# Kubernetes.IoK8sApiStorageV1VolumeAttachmentSource

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inlineVolumeSpec** | [**IoK8sApiCoreV1PersistentVolumeSpec**](IoK8sApiCoreV1PersistentVolumeSpec.md) |  | [optional] 
**persistentVolumeName** | **String** | Name of the persistent volume to attach. | [optional] 


