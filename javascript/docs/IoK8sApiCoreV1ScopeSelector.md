# Kubernetes.IoK8sApiCoreV1ScopeSelector

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**matchExpressions** | [**[IoK8sApiCoreV1ScopedResourceSelectorRequirement]**](IoK8sApiCoreV1ScopedResourceSelectorRequirement.md) | A list of scope selector requirements by scope of the resources. | [optional] 


