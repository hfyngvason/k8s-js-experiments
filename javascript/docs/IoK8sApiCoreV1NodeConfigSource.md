# Kubernetes.IoK8sApiCoreV1NodeConfigSource

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configMap** | [**IoK8sApiCoreV1ConfigMapNodeConfigSource**](IoK8sApiCoreV1ConfigMapNodeConfigSource.md) |  | [optional] 


