# Kubernetes.IoK8sApiNetworkingV1IngressStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loadBalancer** | [**IoK8sApiCoreV1LoadBalancerStatus**](IoK8sApiCoreV1LoadBalancerStatus.md) |  | [optional] 


