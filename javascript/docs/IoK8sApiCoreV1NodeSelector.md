# Kubernetes.IoK8sApiCoreV1NodeSelector

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nodeSelectorTerms** | [**[IoK8sApiCoreV1NodeSelectorTerm]**](IoK8sApiCoreV1NodeSelectorTerm.md) | Required. A list of node selector terms. The terms are ORed. | 


