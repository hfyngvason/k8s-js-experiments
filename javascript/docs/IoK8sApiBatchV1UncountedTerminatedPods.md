# Kubernetes.IoK8sApiBatchV1UncountedTerminatedPods

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**failed** | **[String]** | Failed holds UIDs of failed Pods. | [optional] 
**succeeded** | **[String]** | Succeeded holds UIDs of succeeded Pods. | [optional] 


