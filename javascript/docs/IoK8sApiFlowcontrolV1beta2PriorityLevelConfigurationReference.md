# Kubernetes.IoK8sApiFlowcontrolV1beta2PriorityLevelConfigurationReference

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | &#x60;name&#x60; is the name of the priority level configuration being referenced Required. | 


