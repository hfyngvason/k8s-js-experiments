# Kubernetes.IoK8sApiStorageV1CSINodeSpec

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**drivers** | [**[IoK8sApiStorageV1CSINodeDriver]**](IoK8sApiStorageV1CSINodeDriver.md) | drivers is a list of information of all CSI Drivers existing on a node. If all drivers in the list are uninstalled, this can become empty. | 


