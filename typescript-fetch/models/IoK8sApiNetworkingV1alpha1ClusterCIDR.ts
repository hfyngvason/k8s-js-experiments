/* tslint:disable */
/* eslint-disable */
/**
 * Kubernetes
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: unversioned
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import type { IoK8sApiNetworkingV1alpha1ClusterCIDRSpec } from './IoK8sApiNetworkingV1alpha1ClusterCIDRSpec';
import {
    IoK8sApiNetworkingV1alpha1ClusterCIDRSpecFromJSON,
    IoK8sApiNetworkingV1alpha1ClusterCIDRSpecFromJSONTyped,
    IoK8sApiNetworkingV1alpha1ClusterCIDRSpecToJSON,
} from './IoK8sApiNetworkingV1alpha1ClusterCIDRSpec';
import type { IoK8sApimachineryPkgApisMetaV1ObjectMeta } from './IoK8sApimachineryPkgApisMetaV1ObjectMeta';
import {
    IoK8sApimachineryPkgApisMetaV1ObjectMetaFromJSON,
    IoK8sApimachineryPkgApisMetaV1ObjectMetaFromJSONTyped,
    IoK8sApimachineryPkgApisMetaV1ObjectMetaToJSON,
} from './IoK8sApimachineryPkgApisMetaV1ObjectMeta';

/**
 * ClusterCIDR represents a single configuration for per-Node Pod CIDR allocations when the MultiCIDRRangeAllocator is enabled (see the config for kube-controller-manager).  A cluster may have any number of ClusterCIDR resources, all of which will be considered when allocating a CIDR for a Node.  A ClusterCIDR is eligible to be used for a given Node when the node selector matches the node in question and has free CIDRs to allocate.  In case of multiple matching ClusterCIDR resources, the allocator will attempt to break ties using internal heuristics, but any ClusterCIDR whose node selector matches the Node may be used.
 * @export
 * @interface IoK8sApiNetworkingV1alpha1ClusterCIDR
 */
export interface IoK8sApiNetworkingV1alpha1ClusterCIDR {
    /**
     * APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources
     * @type {string}
     * @memberof IoK8sApiNetworkingV1alpha1ClusterCIDR
     */
    apiVersion?: string;
    /**
     * Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
     * @type {string}
     * @memberof IoK8sApiNetworkingV1alpha1ClusterCIDR
     */
    kind?: string;
    /**
     * 
     * @type {IoK8sApimachineryPkgApisMetaV1ObjectMeta}
     * @memberof IoK8sApiNetworkingV1alpha1ClusterCIDR
     */
    metadata?: IoK8sApimachineryPkgApisMetaV1ObjectMeta;
    /**
     * 
     * @type {IoK8sApiNetworkingV1alpha1ClusterCIDRSpec}
     * @memberof IoK8sApiNetworkingV1alpha1ClusterCIDR
     */
    spec?: IoK8sApiNetworkingV1alpha1ClusterCIDRSpec;
}

/**
 * Check if a given object implements the IoK8sApiNetworkingV1alpha1ClusterCIDR interface.
 */
export function instanceOfIoK8sApiNetworkingV1alpha1ClusterCIDR(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function IoK8sApiNetworkingV1alpha1ClusterCIDRFromJSON(json: any): IoK8sApiNetworkingV1alpha1ClusterCIDR {
    return IoK8sApiNetworkingV1alpha1ClusterCIDRFromJSONTyped(json, false);
}

export function IoK8sApiNetworkingV1alpha1ClusterCIDRFromJSONTyped(json: any, ignoreDiscriminator: boolean): IoK8sApiNetworkingV1alpha1ClusterCIDR {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'apiVersion': !exists(json, 'apiVersion') ? undefined : json['apiVersion'],
        'kind': !exists(json, 'kind') ? undefined : json['kind'],
        'metadata': !exists(json, 'metadata') ? undefined : IoK8sApimachineryPkgApisMetaV1ObjectMetaFromJSON(json['metadata']),
        'spec': !exists(json, 'spec') ? undefined : IoK8sApiNetworkingV1alpha1ClusterCIDRSpecFromJSON(json['spec']),
    };
}

export function IoK8sApiNetworkingV1alpha1ClusterCIDRToJSON(value?: IoK8sApiNetworkingV1alpha1ClusterCIDR | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'apiVersion': value.apiVersion,
        'kind': value.kind,
        'metadata': IoK8sApimachineryPkgApisMetaV1ObjectMetaToJSON(value.metadata),
        'spec': IoK8sApiNetworkingV1alpha1ClusterCIDRSpecToJSON(value.spec),
    };
}

