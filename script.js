kubernetes.ApiClient.instance.basePath = 'http://localhost:8002'

const coreV1 = new kubernetes.CoreV1Api()

const podRow = (pod) => {
  console.log(pod);
  const tr = document.createElement('tr');
  const tdName = document.createElement('td');
  const tdNamespace = document.createElement('td');
  tdName.innerHTML = pod.metadata.name;
  tdNamespace.innerHTML = pod.metadata.namespace;
  tr.appendChild(tdName);
  tr.appendChild(tdNamespace);
  console.log(tr.outerHTML);
  return tr;
};

coreV1.listCoreV1PodForAllNamespaces({}, (error, data, result) => {
  if (error) {
    console.error(error);
    return;
  }
  const tbody = document.getElementById('pods-tbody');
  data.items.forEach(pod => {
    tbody.appendChild(podRow(pod));
  });
});
