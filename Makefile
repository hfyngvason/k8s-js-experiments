swagger.json:
	wget "https://raw.githubusercontent.com/kubernetes/kubernetes/master/api/openapi-spec/swagger.json" -O swagger.json

.PHONY: javascript
javascript: swagger.json
	openapi-generator generate -i swagger.json -g javascript -o javascript

javascript/dist/index.js: javascript
	cd javascript && npm run build

# TODO: Try this out. It uses the fetch API, so the client might be nicer
.PHONY: typescript-fetch
typescript-fetch: swagger.json
	openapi-generator generate -i swagger.json -g typescript-fetch -o typescript-fetch

bundle.js: main.js javascript
	browserify main.js > bundle.js
